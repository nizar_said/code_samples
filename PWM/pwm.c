/* pwm.c
 *
 *
 * Copyright (c)2015 comelit R&D Tunisia
 *
 */

#include "pwm_cred.h"

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that enables the pwm device
 * @Param: device chip
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
{
	
	FILE *p=NULL;
	int err;
	/*Get access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/export","w");
		
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
 	{
		p = fopen("/sys/class/pwm/pwmchip1/export","w");
		
	}
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",0);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device or resource busy\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("PWM Successfully Initialized \n");
	return CRED_NO_ERROR;
}

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that closes the pwm device
 * @Param: device chip
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_Term(CRED_pwm_device_t pwm_device)
{
	
	FILE *p=NULL;
	int err;
	
	/*Block access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/unexport","w");
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
		p = fopen("/sys/class/pwm/pwmchip1/unexport","w");
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",0);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("PWM device Closed\n");
	return CRED_NO_ERROR;
}

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that activates the pwm device
 * @Param: device chip
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_Enable(CRED_pwm_device_t pwm_device)
{
	FILE *p=NULL;
	int err;
	
	/*Block access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/pwm0/enable","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
 	{
		p = fopen("/sys/class/pwm/pwmchip1/pwm0/enable","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}

	}
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",1);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("PWM Enabled Successfully \n");
	return CRED_NO_ERROR;

}

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that deactivates the pwm device
 * @Param: device chip
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_Disable(CRED_pwm_device_t pwm_device)
{
	FILE *p=NULL;
	int err;
	
	/*Block access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/pwm0/enable","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
 	{
		p = fopen("/sys/class/pwm/pwmchip1/pwm0/enable","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}

	}
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",0);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("PWM Disabled Successfully \n");
	return CRED_NO_ERROR;
}

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that sets the frequence the pwm device
 * @Param: device chip
 *        period: device frequency.
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_SetPeriod(CRED_pwm_device_t pwm_device, int period)
{

	FILE *p=NULL;
	int err;
	
	/*Block access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/pwm0/period","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
 	{
		p = fopen("/sys/class/pwm/pwmchip1/pwm0/period","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}

	}
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",period);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("Period Successfully updated \n");
	return CRED_NO_ERROR;
}

/**
 * @Name:CRED_Errors_t PWM_Init(CRED_pwm_device_t pwm_device)
 * @Brief: Function that sets the duty cycle of the pwm device
 * @Param: device chip
 *        duty Cycle
 * @Return: CRED_Errors_t: CRED_N_ERROR on sucess other in failure
 */ 	
CRED_Errors_t PWM_SetDutyCycle(CRED_pwm_device_t pwm_device, int dutyCycle)
{

	FILE *p=NULL;
	int err;
	
	/*Block access to the desired pwm chip*/
	if(pwm_device==CRED_PWM_PWMCHIP0)
	{
		p = fopen("/sys/class/pwm/pwmchip0/pwm0/period","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}
	}
 	else if(pwm_device==CRED_PWM_PWMCHIP1)
 	{
		p = fopen("/sys/class/pwm/pwmchip1/pwm0/period","w");
		if(p==NULL)
		{
			 PWM_TRACE_ERROR("No such Device\n");
			 return CRED_ERROR_BAD_PARAMETER;
	
		}

	}
 	else
 	{
		PWM_TRACE_ERROR("PWM Chip not recognized\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	err=fprintf(p,"%d",dutyCycle);
	if(err<0)
	{
	 PWM_TRACE_ERROR("No such Device\n");
	 return CRED_ERROR_BAD_PARAMETER;

	}
	fclose(p);
	PWM_TRACE_INFO("PWM Duty Cycle Successfully Updated \n");
	return CRED_NO_ERROR;
}

/*NOT USED*/
/**
 * @Name:
 * @Brief:
 * @Param:
 * @Return:
 */ 	
CRED_Errors_t PWM_WRITE(int fd_pwm,int frequence,int duration,int duty,int pos_length ,int device)
{
	char f[5],dur[5],dut[3],l[4],dev[2];
	char* buffer =NULL;

	if (frequence <= 0 || frequence >=30000)
	{	
		PWM_TRACE_ERROR("Wrong frequency value \n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (duration <= 0)
	{
		PWM_TRACE_ERROR("Wrong duration value \n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (duty < 0 || duty >100)
	{		
		PWM_TRACE_ERROR("Wrong duty value ");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pos_length < 0)
	{
		PWM_TRACE_ERROR("Wrong pos_length value \n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (device != 0 && device != 1)
	{
		PWM_TRACE_ERROR("Wrong device \n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	buffer = malloc(20);
	
	if (buffer == NULL) 
	{
		PWM_TRACE_ERROR("malloc() buffer \n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	memset((void*)buffer,0,20);	
	
	sprintf(f,"%d",frequence);
	strcpy(buffer,f);
	strcat(buffer,"\n");
	
	sprintf(dur,"%d",duration);
	strcat(buffer,dur);
	strcat(buffer,"\n");
	
	sprintf(dut,"%d",duty);
	strcat(buffer,dut);
	strcat(buffer,"\n");
	
	sprintf(l,"%d",pos_length);
	strcat(buffer,l);
	strcat(buffer,"\n");
	
	sprintf(dev,"%d",device);
	strcat(buffer,dev);
	strcat(buffer,"\n");
	
	if (write(fd_pwm,buffer,20) != 0)
	{
		PWM_TRACE_ERROR("Device Error \n");
		if (buffer != NULL) 
		{
			free(buffer);
		}
		return CRED_ERROR_DEVICE;
	}
	
	if (buffer != NULL) 
	{
		free(buffer);
	}
	return CRED_NO_ERROR;
	
}

