
#if 0
/* uart.c
 *
 *
 * Copyright (c)2015 comelit R&D Tunisia
 *
 */

#include "output_cred.h"

int output_fd;
pthread_t 	OUTPUT_ThreadWrite;
pthread_t 	dtmf_ThreadWrite;
CRED_output_buffer_t OutputBuffer;
int KeepOn = 0;
int flush_buffer = 0;

void OUTPUT_push()
{
	int buffer_size = 0 , LSize = 0 , RSize = 0;
	int i = 0;
	char buffer[OUTPUT_MAX_SIZE_TO_WRITE];

	memset(buffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	
	while(KeepOn)
	{		
		
		sem_wait(&OutputBuffer.OutputSem);

		while((OutputBuffer.left_size > 0) || (OutputBuffer.right_size > 0))
		{
			buffer_size = 0;

			LSize = OutputBuffer.left_size;
			RSize = OutputBuffer.right_size;

			if(RSize < LSize)
			{ 
				if(RSize > 0)
				{
					buffer_size = RSize;
				}
				else
				{
					buffer_size = LSize; 
				}
			}
			else
			{
				if(LSize > 0)
				{
					buffer_size = LSize;
				}
				else
				{
					buffer_size = RSize; 
				}
			}
			
			if(buffer_size > 8192)
			buffer_size = 8192;
			
			if(OutputBuffer.left_size > 0)
			{
				OutputBuffer.left_size = OutputBuffer.left_size - buffer_size;
			}
			
			if(OutputBuffer.right_size > 0)
			{
				OutputBuffer.right_size = OutputBuffer.right_size - buffer_size;
			}
			
			i = 0;

			if (buffer_size > 0)
			{

				if(RSize > 0)
				{
					if((OutputBuffer.right_read + buffer_size) > OUTPUT_MAX_SIZE_TO_WRITE)
					{
						memcpy(buffer, (OutputBuffer.rightbuffer + OutputBuffer.right_read), (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
						memcpy(buffer +  (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read), OutputBuffer.rightbuffer , buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
						
						OutputBuffer.right_read = buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read);
					}
					else
					{
						memcpy(buffer , (OutputBuffer.rightbuffer + OutputBuffer.right_read), buffer_size);
						
						OutputBuffer.right_read = OutputBuffer.right_read + buffer_size;
					}
				}
				

				while(i < buffer_size)
				{

					if((OutputBuffer.left_read) > OUTPUT_MAX_SIZE_TO_WRITE)
					{
						OutputBuffer.left_read = 0;
					}
					
					if(LSize > 0)
					{
						buffer[i] = *(OutputBuffer.leftbuffer + OutputBuffer.left_read);
						buffer[i + 1] = *(OutputBuffer.leftbuffer + OutputBuffer.left_read + 1);
						
						OutputBuffer.left_read = OutputBuffer.left_read + 4;
					}
					else
					{
						buffer[i] = 0x00;
						buffer[i + 1] = 0x00;
						
					}
					
					if((OutputBuffer.left_read + 1) > OUTPUT_MAX_SIZE_TO_WRITE)
					{
						OutputBuffer.left_read = 0 ;
					}
					
					i = i + 4;
				}
				

				write(output_fd, buffer, buffer_size);

				memset(buffer, 0x00 , buffer_size);
			}
			
			usleep(1000);
			if(((OutputBuffer.left_size == 0) && (OutputBuffer.right_size == 0)) || (( flush_buffer == 1) && (OutputBuffer.right_size == 0) ))
			{
				flush_buffer = 0;
				OUTPUT_Flush();
			}
		}
		OUTPUT_TRACE_INFO("OutputBuffer.left_write %d OutputBuffer.left_read %d OutputBuffer.left_size %d LSize %d !!\n",OutputBuffer.left_write,OutputBuffer.left_read,OutputBuffer.left_size,LSize);
	}
} 

/*! \fn CRED_Errors_t OUTPUT_Init()
    \brief Initialyze the OUTPUT driver.
*/

CRED_Errors_t OUTPUT_GetDspFd(int* dsp_fd)
{
	if(output_fd<0)
	{
		return CRED_ERROR_BAD_FD;	
	}
	
	*dsp_fd=output_fd;
	return CRED_NO_ERROR;	
}
/*! \fn CRED_Errors_t OUTPUT_Init()
    \brief Initialyze the OUTPUT driver.
*/

CRED_Errors_t OUTPUT_Init()
{
	int channel = 2;
	int SampleRate = 11025;
	const int i32OSS_Format = AFMT_S16_LE;
	CRED_Errors_t err = CRED_NO_ERROR;

	memset(OutputBuffer.leftbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	memset(OutputBuffer.rightbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	
	OutputBuffer.left_write = 0;
	OutputBuffer.left_read = 0;
	OutputBuffer.left_size = 0;
	OutputBuffer.right_write = 0;
	OutputBuffer.right_read = 0;
	OutputBuffer.right_size = 0;
	
	sem_init(&OutputBuffer.OutputSem, 0, 0);
	
	//~ output_fd = open("/dev/dsp", O_RDWR|O_SYNC);
	output_fd = open("/dev/audio", O_RDWR|O_SYNC);

	if (output_fd < 0)
	{
		output_fd = open("/dev/dsp2", O_RDWR|O_SYNC);
		
		if (output_fd < 0)
		{
			OUTPUT_TRACE_ERROR( "Failed to open any dsp device\n");
			return CRED_ERROR_BAD_FD;
		} 
	}
	
	if (ioctl(output_fd, SNDCTL_DSP_SETFMT, &i32OSS_Format)== -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_SETFMT ioctl error \n");	 
	}
	
	if(ioctl(output_fd, SNDCTL_DSP_SPEED, &SampleRate)== -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_SPEED ioctl error\n");
	}
	
	if(ioctl(output_fd, SNDCTL_DSP_CHANNELS, &channel) == -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_CHANNELS ioctl error\n");
	}
	
	KeepOn = 1;
		
	err = CRED_TOOLS_CreateTask("GPIO_Write", &OUTPUT_ThreadWrite, 0x2000,OUTPUT_push, 99, SCHED_FIFO,NULL) ;
	if ( err < 0)
	{
		OUTPUT_TRACE_ERROR("Uneable to create alarm thread1 !!\n");
		return CRED_ERROR_DEVICE;
	}

	return err;

}

/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/

 CRED_Errors_t OUTPUT_Term()
 {
	 CRED_Errors_t err;
	 
	 KeepOn = 0;
	 
	 sem_post(&OutputBuffer.OutputSem);
	 
	 err = close(output_fd);	
	
	if (err < 0)
	{
		OUTPUT_TRACE_ERROR("Unable to close device\n");
		return CRED_ERROR_DEVICE;
	}
	
	sem_destroy(&OutputBuffer.OutputSem);
	
	return err;
 }
/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/

CRED_Errors_t OUTPUT_right_Term()
{
	CRED_Errors_t err = CRED_NO_ERROR;
	memset(OutputBuffer.rightbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	OutputBuffer.right_write = 0;
	OutputBuffer.right_read = 0;
	OutputBuffer.right_size = 0;
	//RSize=0;
	return err;
 }
/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/
CRED_Errors_t OUTPUT_left_Term()
{
	CRED_Errors_t err = CRED_NO_ERROR;
	memset(OutputBuffer.leftbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	OutputBuffer.left_write = 0;
	OutputBuffer.left_read = 0;
	OutputBuffer.left_size = 0;
	//LSize=0;
	return err;
 }
 
/*! \fn CRED_Errors_t OUTPUT_Flush()
    \brief Terminate the OUTOUT Driver.
*/

 CRED_Errors_t OUTPUT_Flush()
 {
	 pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
	 
	if(ioctl(output_fd, SNDCTL_DSP_SYNC, NULL) == -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_SYNC ioctl error\n");
	}

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);

	//OUTPUT_TRACE_ERROR("OUTPUT_Flush \n");
	
	return 0;
 }
 
 /*! \fn CRED_Errors_t OUTPUT_Write(CRED_output_channel_t channel,char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/

 CRED_Errors_t OUTPUT_Write(CRED_output_channel_t channel,char *buf, int buff_size)
{
	
	if(channel == CRED_OUTPUT_LEFT_CHANNEL)
	{
		if((OutputBuffer.left_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf, (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
			memcpy(OutputBuffer.leftbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
			
			OutputBuffer.left_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write);
		}
		else
		{
			memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf ,buff_size);
			
			OutputBuffer.left_write = OutputBuffer.left_write + buff_size;
		}
		
		if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			OutputBuffer.left_size = OUTPUT_MAX_SIZE_TO_WRITE;
		}
		else
		{
			OutputBuffer.left_size = OutputBuffer.left_size + buff_size;
		}
	}
	else
	{		
		if((OutputBuffer.right_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf , (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
			memcpy(OutputBuffer.rightbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
			
			OutputBuffer.right_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write);
		}
		else
		{
			memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf ,buff_size);
			
			OutputBuffer.right_write = OutputBuffer.right_write + buff_size;
		}
		
		if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			OutputBuffer.right_size = OUTPUT_MAX_SIZE_TO_WRITE;
		}
		else
		{
			OutputBuffer.right_size = OutputBuffer.right_size + buff_size;
		}
	}

	sem_post(&OutputBuffer.OutputSem);

	return 0;
}


 /*! \fn CRED_Errors_t OUTPUT_RWrite(char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/

 CRED_Errors_t OUTPUT_RWrite(char *buf, int buff_size)
{
	//flush_buffer = 1;
		
	if((OutputBuffer.right_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf , (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
		memcpy(OutputBuffer.rightbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
		
		OutputBuffer.right_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write);
	}
	else
	{
		memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf ,buff_size);
		
		OutputBuffer.right_write = OutputBuffer.right_write + buff_size;
	}

	if((OutputBuffer.right_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		OutputBuffer.right_size = OUTPUT_MAX_SIZE_TO_WRITE;
	}
	else
	{
		OutputBuffer.right_size = OutputBuffer.right_size + buff_size;
	}

	sem_post(&OutputBuffer.OutputSem);

	return 0;
}

 /*! \fn CRED_Errors_t OUTPUT_LWrite(char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/

 CRED_Errors_t OUTPUT_LWrite(char *buf, int buff_size)
{
	if((OutputBuffer.left_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf, (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
		memcpy(OutputBuffer.leftbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
		
		OutputBuffer.left_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write);
	}
	else
	{
		memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf ,buff_size);
		
		OutputBuffer.left_write = OutputBuffer.left_write + buff_size;
	}
	
	if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		OutputBuffer.left_size = OUTPUT_MAX_SIZE_TO_WRITE;
	}
	else
	{
		OutputBuffer.left_size = OutputBuffer.left_size + buff_size;
	}

	sem_post(&OutputBuffer.OutputSem);

	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_SetSampleRate(int *Sample)
    \brief Set sample rate.
    \param Sample sample rate value.
*/

 CRED_Errors_t OUTPUT_SetSampleRate(int *Sample)
{
	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);	
	
	if(ioctl(output_fd, SNDCTL_DSP_SPEED, Sample)== -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_SPEED ioctl error\n");
	}
	
	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);	
	
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_GetBlockSize(int *BlockSize)
    \brief Get block size.
    \param BlockSize block size.
*/

 CRED_Errors_t OUTPUT_GetBlockSize(int *BlockSize)
{
	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);	

	if(ioctl(output_fd, SNDCTL_DSP_GETBLKSIZE, BlockSize) == -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_GETBLKSIZE ioctl error\n");
	}

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);	
		
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_GetOSpace(audio_buf_info *DSP_Info)
    \brief Get space.
    \param DSP_Info free space.
*/

 CRED_Errors_t OUTPUT_GetOSpace(audio_buf_info *DSP_Info)
{

	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
	
	if(ioctl(output_fd, SNDCTL_DSP_GETOSPACE, DSP_Info) == -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_GETOSPACE ioctl error\n");
	}
	
	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);
	
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_LGetFreeBufferSize()
    \brief Get space.
    \param DSP_Info free space.
*/

int OUTPUT_LGetFreeBufferSize()
{
	return (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_size);
}

/*! \fn CRED_Errors_t OUTPUT_RGetFreeBufferSize()
    \brief Get space.
    \param DSP_Info free space.
*/

int OUTPUT_RGetFreeBufferSize()
{
	return (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_size);
}


#else

/**=========== output drivers by ALSA APIs ===============*/
/* uart.c
 *
 *
 * Copyright (c)2015 comelit R&D Tunisia
 *
 */

#include "output_cred.h"

#define PCM_LEFT_CHANNEL "default"  						 ///< The name of left audio device.
#define PCM_RIGHT_CHANNEL "card2"  						     ///< The name of Right audio device.

int output_fd = -1;
pthread_t 	OUTPUT_ThreadLeftWrite,OUTPUT_ThreadRightWrite;
pthread_t 	dtmf_ThreadWrite;
CRED_output_buffer_t OutputBuffer;
static int currentchannelNum;   					 ///< Channel Num =2 in most cases
static int currentSampleRate;                        ///< Audio Sample Rate 
static snd_pcm_uframes_t currentPeriodSize;          ///<  Period Size
static snd_pcm_format_t currentAlsaPcmFormat;
static  snd_pcm_t *playbackLeft=NULL, *playbackRight=NULL;              ///< Handle pointer for audio playback hardware.
static int bytesPerFrame;                            ///< Number of bytes per each frame.
int KeepOn = 0;
int flush_buffer = 0;


CRED_Errors_t OUTPUT_GetDspFd(int* dsp_fd)
{
	if(output_fd<0)
	{
		return CRED_ERROR_BAD_FD;	
	}
	
	*dsp_fd=output_fd;
	return CRED_NO_ERROR;	
}


/*! \fn CRED_Errors_t OUTPUT_GetBytesPerFrame(int *BytesNum,snd_pcm_format_t PcmFormat)
    \brief Function to get the number of bytes per each frame, used to seperate the left and right stream.
    \param int *BytesNum  byte number
    \param  snd_pcm_format_t PcmFormat sound card pcm format.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t OUTPUT_GetBytesPerFrame(int *BytesNum,snd_pcm_format_t PcmFormat)
{
	
	 if((int)PcmFormat<5)
			*BytesNum=2;
     else if ((int)PcmFormat<9)
          *BytesNum=3;
     else
             *BytesNum=4;
	return CRED_NO_ERROR;
}

/*! \fn OUTPUT_ExtSpeaker_SwitchOn(bool enable)
    \brief Function to enables/disables the external speaker.
    \param bool enable or disable.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
void OUTPUT_ExtSpeaker_SwitchOn(bool enable)
{
    //~ FILE *p=NULL;
 	//~ p = fopen("/sys/class/gpio/gpio43/value","w");
 	//~ 
 	//~ int value = enable ? 0 : 1; // shutdown high means spk off
	//~ fprintf(p,"%d",value);
 //~ 
    //~ fclose(p);
}

/*! \fn CRED_Errors_t OUTPUT_GetPcmState(snd_pcm_state_t* pcm_state)
    \brief Function to get the pcm state.
    \param snd_pcm_state_t *pcm state
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t OUTPUT_GetPcmState(snd_pcm_state_t* pcm_state)
{
   *pcm_state=snd_pcm_state(playbackLeft);
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_PlaybackHardware_Prepare(snd_pcm_t *handle)
    \brief Function to prepare the alsa playback hardware.
    \param snd_pcm_t *handle playback handler
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t OUTPUT_PlaybackHardware_Left_Prepare()
{
	
	/* Prepare playback */
	if(playbackLeft==NULL)
	{
		OUTPUT_TRACE_ERROR("Playback Handler Pointer is Null\n");
		return 0;
	}
	
	//~ if(snd_pcm_state(playbackLeft)==SND_PCM_STATE_RUNNING)
	     //~ return CRED_NO_ERROR; // AUDIO Device is running
	     
    /* drop any output we might got and stop */    	     
    snd_pcm_drop(playbackLeft);
    snd_pcm_reset(playbackLeft);
    
    /* prepare for use */    
    snd_pcm_prepare(playbackLeft);
    
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_PlaybackHardware_Prepare(snd_pcm_t *handle)
    \brief Function to prepare the alsa playback hardware.
    \param snd_pcm_t *handle playback handler
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t OUTPUT_PlaybackHardware_Right_Prepare()
{
	
	/* Prepare playback */
	if(playbackRight==NULL)
	{
		OUTPUT_TRACE_ERROR("Playback Handler Pointer is Null\n");
		return 0;
	}
	
    
    /* drop any output we might got and stop */
    snd_pcm_drop(playbackRight);    
    snd_pcm_reset(playbackRight);
    
    /* prepare for use */    
    snd_pcm_prepare(playbackRight);
    
	return CRED_NO_ERROR;
}
/*! \fn CRED_Errors_t AUDIO_PlaybackHardware_Prepare(snd_pcm_t *handle)
    \brief Function to setup the alsa playback hardware.
    \param enable 0=resume, 1=pause
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t OUTPUT_PlaybackHardware_Left_Pause(int enable)
{
	int err;
	if(err=snd_pcm_pause(playbackLeft,enable)<0)
	    OUTPUT_TRACE_ERROR("Pausing failed \n");
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_PlaybackHardware_Prepare(snd_pcm_t *handle)
    \brief Function to setup the alsa playback hardware.
    \param enable 0=resume, 1=pause
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t OUTPUT_PlaybackHardware_Right_Pause(int enable)
{
	int err;
	
	if((snd_pcm_state(playbackRight)==SND_PCM_STATE_RUNNING && enable)|| 
				(snd_pcm_state(playbackRight)==SND_PCM_STATE_PAUSED && !enable))
	{
	if(err=snd_pcm_pause(playbackRight,enable)<0)
	    OUTPUT_TRACE_ERROR("Pausing failed \n");
	}
	else   OUTPUT_TRACE_ERROR("AUDIO ACTION CAN'T BE TAKEN \n");

	return CRED_NO_ERROR;
}

 /*! \fn CRED_Errors_t OUTPUT_PlaybackHardware_Left_Init(snd_pcm_t *handle)
    \brief Function to setup the alsa left playback hardware.
    \param snd_pcm_t *handle playback handler
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t OUTPUT_PlaybackHardware_Left_Init(snd_pcm_t *handle)
{
	int err,tmp;
	snd_pcm_hw_params_t *params;

		
	/* Open the PCM left device in playback mode */
	if (err = snd_pcm_open(&handle, PCM_LEFT_CHANNEL,
					SND_PCM_STREAM_PLAYBACK, 0) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't open \"%s\" PCM device. %s\n",
					PCM_LEFT_CHANNEL, snd_strerror(err));

    playbackLeft=handle;
    
	/* Allocate parameters object and fill it with default values*/
	snd_pcm_hw_params_alloca(&params);
	snd_pcm_hw_params_any(handle, params);

	/* Set parameters */
	if (err = snd_pcm_hw_params_set_access(handle, params,
					SND_PCM_ACCESS_RW_INTERLEAVED) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set interleaved mode. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_format(handle, params,
						currentAlsaPcmFormat) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set format. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_channels(handle, params, currentchannelNum) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set channels number. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_rate_near(handle, params, &currentSampleRate, 0) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set rate. %s\n", snd_strerror(err));

    if ((err = snd_pcm_hw_params_set_period_size_near(handle, params, &currentPeriodSize, NULL)) < 0)
    {
        OUTPUT_TRACE_ERROR("ERROR: Unable to set period size %i for playback: %s\n", currentPeriodSize, snd_strerror(err));
        return CRED_ERROR_INIT;
    }

	/* Write parameters */
	if (err = snd_pcm_hw_params(handle, params) < 0)
		OUTPUT_TRACE_ERROR("ERROR: Can't set harware parameters. %s\n", snd_strerror(err));

	return CRED_NO_ERROR;
}

 /*! \fn CRED_Errors_t OUTPUT_PlaybackHardware_Right_Init(snd_pcm_t *handle)
    \brief Function to setup the alsa right playback hardware.
    \param snd_pcm_t *handle playback handler
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t OUTPUT_PlaybackHardware_Right_Init(snd_pcm_t *handle)
{
	int err,tmp;
	snd_pcm_hw_params_t *params;

		
	/* Open the right PCM device in playback mode */
	if (err = snd_pcm_open(&handle, PCM_RIGHT_CHANNEL,
					SND_PCM_STREAM_PLAYBACK, 0) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't open \"%s\" PCM device. %s\n",
					PCM_RIGHT_CHANNEL, snd_strerror(err));

    playbackRight=handle;
    
	/* Allocate parameters object and fill it with default values*/
	snd_pcm_hw_params_alloca(&params);

	snd_pcm_hw_params_any(handle, params);

	/* Set parameters */
	if (err = snd_pcm_hw_params_set_access(handle, params,
					SND_PCM_ACCESS_RW_INTERLEAVED) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set interleaved mode. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_format(handle, params,
						currentAlsaPcmFormat) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set format. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_channels(handle, params, currentchannelNum) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set channels number. %s\n", snd_strerror(err));

	if (err = snd_pcm_hw_params_set_rate_near(handle, params, &currentSampleRate, 0) < 0) 
		OUTPUT_TRACE_ERROR("ERROR: Can't set rate. %s\n", snd_strerror(err));

    if ((err = snd_pcm_hw_params_set_period_size_near(handle, params, &currentPeriodSize, NULL)) < 0)
    {
        OUTPUT_TRACE_ERROR("ERROR: Unable to set period size %i for playback: %s\n", currentPeriodSize, snd_strerror(err));
        return CRED_ERROR_INIT;
    }

	/* Write parameters */
	if (err = snd_pcm_hw_params(handle, params) < 0)
		OUTPUT_TRACE_ERROR("ERROR: Can't set harware parameters. %s\n", snd_strerror(err));

	return CRED_NO_ERROR;
}

 
/*! \fn CRED_Errors_t AUDIO_PlaybackHardware_DeInit(snd_pcm_t *handle)
    \brief Function to setup the alsa playback hardware.
    \param snd_pcm_t *handle playback handler
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t OUTPUT_PlaybackHardware_DeInit(snd_pcm_t *handle)
{
	snd_pcm_drain(handle);
	snd_pcm_close(handle);

	
	return CRED_NO_ERROR;
}
/**
 * Function to recovery underrun and suspend.
 * @brief xRunRecovery
 * @param handle
 * @param err
 * @return
 */
CRED_Errors_t AUDIO_xRunRecovery(snd_pcm_t *handle, int err)
{
    if (err == -EPIPE) {    /* under-run */
		OUTPUT_TRACE_ERROR("under-run\n");
    err = snd_pcm_prepare(handle);
    if (err < 0)
        OUTPUT_TRACE_ERROR("Can't recovery from underrun, prepare failed\n");
    return CRED_NO_ERROR;
    } 
    else if (err == -ESTRPIPE) {
				OUTPUT_TRACE_ERROR("ESTRPIPE\n");

    while ((err = snd_pcm_resume(handle)) == -EAGAIN)
        sleep(1);       /* wait until the suspend flag is released */
    if (err < 0) {
        err = snd_pcm_prepare(handle);
        if (err < 0)
           AUDIO_TRACE_ERROR("Can't recovery from suspend, prepare failed\n");
    }
    return CRED_NO_ERROR;
    }
    else if (err==-EBADFD){
	 OUTPUT_TRACE_ERROR("EBADFD\n");

	}
    return CRED_NO_ERROR;
}

/**
 * 
 * 
 */
void OUTPUT_Left_push()
{
	int buffer_size = 0 , LSize = 0 ;
	int i = 0,err,rightBuffer, leftBuffer;
	char buffer[OUTPUT_MAX_SIZE_TO_WRITE];

	memset(buffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	
	while(KeepOn)
	{			
		
		sem_wait(&OutputBuffer.OutputSemLeft);

		while(OutputBuffer.left_size > 0)
		{
			buffer_size = 0;

			LSize = OutputBuffer.left_size;
		
			buffer_size = LSize;
				
			
			if(buffer_size > 4096)
			buffer_size = 4096;
			
			
			if(OutputBuffer.left_size > 0)
			{
				OutputBuffer.left_size = OutputBuffer.left_size - buffer_size;
			}

			i = 0;
			if (buffer_size > 0)
			{


					if((OutputBuffer.left_read + buffer_size) > OUTPUT_MAX_SIZE_TO_WRITE)
					{
							pthread_mutex_lock(&OutputBuffer.g_OutputMutex);

						//memcpy(buffer, (OutputBuffer.rightbuffer + OutputBuffer.right_read), (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
						//memcpy(buffer +  (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read), OutputBuffer.rightbuffer , buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
							for(int j=0; j<buffer_size;j+=bytesPerFrame)
							{
								if((OutputBuffer.left_read+j+bytesPerFrame)>OUTPUT_MAX_SIZE_TO_WRITE)
								{
									memcpy(buffer+j,OutputBuffer.leftbuffer+i,bytesPerFrame);
									i+=bytesPerFrame;
								}
								else
									memcpy(buffer+j,OutputBuffer.leftbuffer + OutputBuffer.left_read+j,bytesPerFrame);
	                         
					        }
					        i=0;
						    OutputBuffer.left_read = buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_read);

					}
					else
					{
							pthread_mutex_lock(&OutputBuffer.g_OutputMutex);

						    //memcpy(buffer , (OutputBuffer.rightbuffer + OutputBuffer.right_read), buffer_size);
							for(int j=0; j<buffer_size;j+=bytesPerFrame)
	                         memcpy(buffer+j,OutputBuffer.leftbuffer + OutputBuffer.left_read+j,bytesPerFrame);


						OutputBuffer.left_read = OutputBuffer.left_read + buffer_size;
					}
					
					
				pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);

	
				 if (err = snd_pcm_writei (playbackLeft, buffer, currentPeriodSize)== -EPIPE)
					{
					AUDIO_TRACE_ERROR("write to audio left channel failed: %d\n",err);
					AUDIO_xRunRecovery(playbackLeft,err);
				    }
								//write(output_fd, buffer, buffer_size);

				memset(buffer, 0x00, buffer_size);
			    //~ printf("read pointer : %d, write pointer: %d",OutputBuffer.left_read,OutputBuffer.left_write);

				

				
			}
			
			usleep(1000);
			if((OutputBuffer.left_size == 0) || ( flush_buffer == 1 ))
			{
				flush_buffer = 0;
				OUTPUT_PlaybackHardware_Left_Prepare();
				
				//OUTPUT_Flush();
			}
		}
		//~ OUTPUT_TRACE_INFO("OutputBuffer.left_write %d OutputBuffer.left_read %d OutputBuffer.left_size %d LSize %d !!\n",OutputBuffer.left_write,OutputBuffer.left_read,OutputBuffer.left_size,LSize);
	}
} 

void OUTPUT_Right_push()
{
	int buffer_size = 0 , RSize = 0;
	int i = 0,err,rightBuffer;
	char buffer[OUTPUT_MAX_SIZE_TO_WRITE];

	memset(buffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	
	while(KeepOn)
	{			
		
		sem_wait(&OutputBuffer.OutputSemRight);

		while(OutputBuffer.right_size > 0)
		{
			buffer_size = 0;

			RSize = OutputBuffer.right_size;

			buffer_size = RSize; 
							
			if(buffer_size > 4096)
			buffer_size = 4096;
			
			OutputBuffer.right_size = OutputBuffer.right_size - buffer_size;
			
			i = 0;
			if (buffer_size > 0)
			{

					if((OutputBuffer.right_read + buffer_size) > OUTPUT_MAX_SIZE_TO_WRITE)
					{
							pthread_mutex_lock(&OutputBuffer.g_OutputMutex);

						//memcpy(buffer, (OutputBuffer.rightbuffer + OutputBuffer.right_read), (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
						// memcpy(buffer +  (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read), OutputBuffer.rightbuffer , buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read));
							for(int j=0; j<buffer_size;j+=bytesPerFrame)
							{
								if((OutputBuffer.right_read+j+bytesPerFrame)>OUTPUT_MAX_SIZE_TO_WRITE)
								{
									memcpy(buffer+j,OutputBuffer.rightbuffer+i,bytesPerFrame);
									i+=bytesPerFrame;
								}
								else
									memcpy(buffer+j,OutputBuffer.rightbuffer + OutputBuffer.right_read+j,bytesPerFrame);
	                         
					        }
					        i=0;
						    OutputBuffer.right_read = buffer_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_read);

					}
					else
					{
							pthread_mutex_lock(&OutputBuffer.g_OutputMutex);

						//memcpy(buffer , (OutputBuffer.rightbuffer + OutputBuffer.right_read), buffer_size);
							for(int j=0; j<buffer_size;j+=bytesPerFrame)
	                         memcpy(buffer+j,OutputBuffer.rightbuffer + OutputBuffer.right_read+j,bytesPerFrame);


						OutputBuffer.right_read = OutputBuffer.right_read + buffer_size;
					}
					
												
						
				pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);
		
				 if (err = snd_pcm_writei (playbackRight, buffer, currentPeriodSize)== -EPIPE)
					{
					AUDIO_TRACE_ERROR("write to audio right channel failed: %d\n",err);
					AUDIO_xRunRecovery(playbackRight,err);
				    }
								//write(output_fd, buffer, buffer_size);

				memset(buffer, 0x00, buffer_size);
				

				}
	
			usleep(1000);
			if((OutputBuffer.right_size == 0) || ( flush_buffer == 1))
			{
				flush_buffer = 0;
				//~ OUTPUT_Flush();
				OUTPUT_PlaybackHardware_Right_Prepare();
				
			}
		}
		//~ OUTPUT_TRACE_INFO("OutputBuffer.left_write %d OutputBuffer.left_read %d OutputBuffer.left_size %d LSize %d !!\n",OutputBuffer.left_write,OutputBuffer.left_read,OutputBuffer.left_size,LSize);
	}
} 

/*! \fn CRED_Errors_t OUTPUT_Init()
    \brief Initialyze the OUTPUT driver.
*/

CRED_Errors_t OUTPUT_Init()
{
	//~ int channel = 2;
	//~ int SampleRate = 11025;
	//~ const int i32OSS_Format = AFMT_S16_LE;
	//~ CRED_Errors_t err = CRED_NO_ERROR;
	//~ 
	//snd_pcm_t *handle;
	//snd_pcm_hw_params_t *params;

	memset(OutputBuffer.leftbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	memset(OutputBuffer.rightbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);

	OutputBuffer.left_write = 0;
	OutputBuffer.left_read = 0;
	OutputBuffer.left_size = 0;
	OutputBuffer.right_write = 0;
	OutputBuffer.right_read = 0;
	OutputBuffer.right_size = 0;
	sem_init(&OutputBuffer.OutputSemLeft, 0, 0);
	sem_init(&OutputBuffer.OutputSemRight, 0, 0);

	KeepOn = 1;
    int err;
    currentAlsaPcmFormat=SND_PCM_FORMAT_S16_LE;          ///< default audio pcm format
    currentchannelNum = 1;
	currentSampleRate= 22050;
    currentPeriodSize=2048;
    OUTPUT_GetBytesPerFrame(&bytesPerFrame,currentAlsaPcmFormat);
    OUTPUT_PlaybackHardware_Left_Init(playbackLeft);
	OUTPUT_PlaybackHardware_Right_Init(playbackRight);
	
	/* Enable external speaker */
	OUTPUT_ExtSpeaker_SwitchOn(1);

	err = CRED_TOOLS_CreateTask("Right_Push", &OUTPUT_ThreadRightWrite, 0x2000,OUTPUT_Right_push, 99, SCHED_FIFO,NULL) ;
	if ( err < 0)
	{
		OUTPUT_TRACE_ERROR("Unable to create alarm thread1 !!\n");
		return CRED_ERROR_DEVICE;
	}
	err = CRED_TOOLS_CreateTask("Left_Push", &OUTPUT_ThreadLeftWrite, 0x2000,OUTPUT_Left_push, 99, SCHED_FIFO,NULL) ;
	if ( err < 0)
	{
		OUTPUT_TRACE_ERROR("Unable to create alarm thread1 !!\n");
		return CRED_ERROR_DEVICE;
	}

	return err;

}

/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/

 CRED_Errors_t OUTPUT_Term()
 {
	 CRED_Errors_t err;
	 
	 KeepOn = 0;
	 
	 sem_post(&OutputBuffer.OutputSemLeft);
	 sem_post(&OutputBuffer.OutputSemRight);
	 
	 //err = close(output_fd);	
	snd_pcm_hw_free(playbackLeft);
    snd_pcm_close (playbackLeft);
    //~ snd_pcm_hw_params_free(params);
	snd_pcm_hw_free(playbackRight);
    snd_pcm_close (playbackRight);

	
	//~ if (err < 0)
	//~ {
		//~ OUTPUT_TRACE_ERROR("Unable to close device\n");
		//~ return CRED_ERROR_DEVICE;
	//~ }
	
	/* Enable external speaker */
	OUTPUT_ExtSpeaker_SwitchOn(0);	

	sem_destroy(&OutputBuffer.OutputSemLeft);
    sem_destroy(&OutputBuffer.OutputSemRight);

	return err;
 }
/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/

CRED_Errors_t OUTPUT_right_Term()
{
	CRED_Errors_t err = CRED_NO_ERROR;
	memset(OutputBuffer.rightbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	OutputBuffer.right_write = 0;
	OutputBuffer.right_read = 0;
	OutputBuffer.right_size = 0;

	//RSize=0;
	return err;
 }
/*! \fn CRED_Errors_t OUTPUT_Term()
    \brief Terminate the OUTOUT Driver.
*/
CRED_Errors_t OUTPUT_left_Term()
{
	CRED_Errors_t err = CRED_NO_ERROR;
	memset(OutputBuffer.leftbuffer,0x00,OUTPUT_MAX_SIZE_TO_WRITE);
	OutputBuffer.left_write = 0;
	OutputBuffer.left_read = 0;
	OutputBuffer.left_size = 0;
	//LSize=0;
	return err;
 }
 
/*! \fn CRED_Errors_t OUTPUT_Flush()
    \brief Terminate the OUTOUT Driver.
*/

 CRED_Errors_t OUTPUT_Flush()
 {
	 pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
	 
	//~ if(ioctl(output_fd, SNDCTL_DSP_SYNC, NULL) == -1)
	//~ {
	  //~ OUTPUT_TRACE_ERROR("SNDCTL_DSP_SYNC ioctl error\n");
	//~ }
    //~ OUTPUT_PlaybackHardware_Prepare();
	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);

	//OUTPUT_TRACE_ERROR("OUTPUT_Flush \n");
	
	return 0;
 }
 
 /*! \fn CRED_Errors_t OUTPUT_Write(CRED_output_channel_t channel,char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/
 CRED_Errors_t OUTPUT_Write(CRED_output_channel_t channel,char *buf, int buff_size)
{
	
	if(channel == CRED_OUTPUT_LEFT_CHANNEL)
	{
		if((OutputBuffer.left_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf, (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
			memcpy(OutputBuffer.leftbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
			
			OutputBuffer.left_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write);
		}
		else
		{
			memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf ,buff_size);
			
			OutputBuffer.left_write = OutputBuffer.left_write + buff_size;
		}
		
		if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			OutputBuffer.left_size = OUTPUT_MAX_SIZE_TO_WRITE;
		}
		else
		{
			OutputBuffer.left_size = OutputBuffer.left_size + buff_size;
		}
	}
	else
	{		
		if((OutputBuffer.right_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf , (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
			memcpy(OutputBuffer.rightbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
			
			OutputBuffer.right_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write);
		}
		else
		{
			memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf ,buff_size);
			
			OutputBuffer.right_write = OutputBuffer.right_write + buff_size;
		}
		
		if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
		{
			OutputBuffer.right_size = OUTPUT_MAX_SIZE_TO_WRITE;
		}
		else
		{
			OutputBuffer.right_size = OutputBuffer.right_size + buff_size;
		}
	}

	sem_post(&OutputBuffer.OutputSemLeft);

	return 0;
}


 /*! \fn CRED_Errors_t OUTPUT_RWrite(char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/

CRED_Errors_t OUTPUT_RWrite(char *buf, int buff_size)
{
	 int err,outputbufferLength;
	
	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
	if((OutputBuffer.right_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf , (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
		memcpy(OutputBuffer.rightbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write));
		
		OutputBuffer.right_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_write);
	}
	else
	{
		memcpy((OutputBuffer.rightbuffer + OutputBuffer.right_write) , buf ,buff_size);
		
		OutputBuffer.right_write = OutputBuffer.right_write + buff_size;
	}
	
	if((OutputBuffer.right_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		OutputBuffer.right_size = OUTPUT_MAX_SIZE_TO_WRITE;
	}
	else
	{
		OutputBuffer.right_size = OutputBuffer.right_size + buff_size;
	}
    //~ OUTPUT_SetBufferLength(outputbufferLength+buff_size, 1);

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);
	
	sem_post(&OutputBuffer.OutputSemRight);
	
	//~ 
	
	//~ char buffer[buff_size*2];
    //~ 
    //~ memset(buffer,0,buff_size*2);
	//~ for(int i=0; i<buff_size;i+=bytesPerFrame)
	    //~ memcpy(buffer+(i*2+bytesPerFrame),buf+i,bytesPerFrame);
		 //~ if ((err = snd_pcm_writei (playbackHandle, buffer, currentPeriodSize)) != (int) currentPeriodSize)
		//~ {
		//~ AUDIO_TRACE_ERROR("write to audio interface failed: %d\n",err);
		//~ AUDIO_xRunRecovery(playbackHandle,err);
		//~ }

	return 0;
	
}

 /*! \fn CRED_Errors_t OUTPUT_LWrite(char *buf, int buff_size)
    \brief Send data through audio output.
    \param channel left or right channel.
    \param *buf Data buffer.
    \param buff_size Data buffer size.
*/
 CRED_Errors_t OUTPUT_LWrite(char *buf, int buff_size)
{
	 int err,outputbufferLength;
	//flush_buffer = 1;
		//~ 
	
	//~ OUTPUT_GetBufferLength(&outputbufferLength, 0);
    //~ while(outputbufferLength+buff_size>OUTPUT_MAX_SIZE_TO_WRITE)
	//~ { 
		//~ struct timespec	sSleepTime = { 0, 0 };
		//~ nanosleep(&sSleepTime, NULL);
	    //~ OUTPUT_GetBufferLength(&outputbufferLength, 0);
//~ 
	//~ }	
	
	if((OutputBuffer.left_write + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf, (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
		memcpy(OutputBuffer.leftbuffer , (buf + OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write) , buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write));
		
		OutputBuffer.left_write = buff_size - (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_write);
	}
	else
	{
		memcpy((OutputBuffer.leftbuffer + OutputBuffer.left_write) , buf ,buff_size);
		
		OutputBuffer.left_write = OutputBuffer.left_write + buff_size;
	}
	
	if((OutputBuffer.left_size + buff_size) > OUTPUT_MAX_SIZE_TO_WRITE)
	{
		OutputBuffer.left_size = OUTPUT_MAX_SIZE_TO_WRITE;
	}
	else
	{
		OutputBuffer.left_size = OutputBuffer.left_size + buff_size;
	}
	
    //~ OUTPUT_SetBufferLength(outputbufferLength+buff_size, 0);

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);

	sem_post(&OutputBuffer.OutputSemLeft);
	
	
    //~ int err;
    //~ char buffer[buff_size*2];
//~ 
    //~ memset(buffer,0,buff_size*2);
	//~ for(int i=0; i<buff_size;i+=bytesPerFrame)
	    //~ memcpy(buffer+(i*2), buf+i, bytesPerFrame);
		 //~ if ((err = snd_pcm_writei (playbackHandle, buffer, currentPeriodSize)) != (int) currentPeriodSize)
		//~ {
		//~ AUDIO_TRACE_ERROR("write to audio interface failed: %d\n",err);
		//~ AUDIO_xRunRecovery(playbackHandle,err);
		//~ }

	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_SetSampleRate(int *Sample)
    \brief Set sample rate.
    \param Sample sample rate value.
*/

 CRED_Errors_t OUTPUT_SetSampleRate(int *Sample)
{
	//~ int err;
	//~ pthread_mutex_lock(&OutputBuffer.g_OutputMutex);	
	//~ printf("PCM state: %s\n", snd_pcm_state_name(snd_pcm_state(playbackHandle)));
//~ 
	//~ if(currentSampleRate!=*Sample)
	//~ {
	   //~ snd_pcm_drop(playbackHandle);
	//~ printf("PCM state: %s\n", snd_pcm_state_name(snd_pcm_state(playbackHandle)));
//~ 
		//~ currentSampleRate=*Sample;
		//~ 
		//~ if (err =snd_pcm_hw_params_set_rate_near(playbackHandle, params, &currentSampleRate, 0) < 0) 
		//~ printf("ERROR: Can't set rate %d. %s\n", currentSampleRate,snd_strerror(err));
//~ 
	   //~ if (err = snd_pcm_hw_params(playbackHandle, params)< 0)
					//~ printf("ERROR: Can't update harware parameters. %s\n", snd_strerror(err));
//~ 
	//~ }
    //~ snd_pcm_hw_params_get_rate(params, &err, 0);
	//~ printf("rate: %d bps\n", err);
	//~ printf("PCM state: %s\n", snd_pcm_state_name(snd_pcm_state(playbackHandle)));
//~ 
	//~ pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);	
	
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_SetSampleRate(int *Sample)
    \brief Set sample rate.
    \param Sample sample rate value.
*/

 CRED_Errors_t OUTPUT_SetParameters(int channelNumber,int Sample, snd_pcm_uframes_t periodSize,snd_pcm_format_t pcmFormat)
{
	int err;
	snd_pcm_state_t AudioPlayeState;

	//~ snd_pcm_hw_params_t *params;
	//~ snd_pcm_t *handle;
    //~ 
   OUTPUT_GetPcmState(&AudioPlayeState);
   while(AudioPlayeState==SND_PCM_STATE_RUNNING)
   {
		  usleep(10000);
		  OUTPUT_GetPcmState(&AudioPlayeState);

   }
	 
	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);	
	
	if((currentchannelNum!=2) || ((int) currentPeriodSize!=(int)periodSize) 
	      ||((int) currentAlsaPcmFormat!=(int)pcmFormat) || (currentSampleRate!=Sample))
	{
		
		/* reset parameters*/
		OUTPUT_PlaybackHardware_DeInit(playbackLeft);
		
		/* set local variables*/
        playbackLeft=NULL;
		currentchannelNum=2;
		currentSampleRate=Sample;
		currentPeriodSize=periodSize;
		currentAlsaPcmFormat=pcmFormat;
		OUTPUT_GetBytesPerFrame(&bytesPerFrame,currentAlsaPcmFormat);

		/* reinitialize audio device*/
	    OUTPUT_PlaybackHardware_Left_Init(playbackLeft);
	}
	
	//~ snd_pcm_hw_params_get_channels(params, &err);
	//~ printf("channels: %i ", err);
//~ 
	//~ snd_pcm_hw_params_get_rate(params, &err, 0);
	//~ printf("rate: %d bps\n", err);

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);	
	
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_GetBlockSize(int *BlockSize)
    \brief Get block size.
    \param BlockSize block size.
*/

 CRED_Errors_t OUTPUT_GetBlockSize(int *BlockSize)
{
	int sampleSz;
	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
		
//~ 
	//~ if(ioctl(output_fd, SNDCTL_DSP_GETBLKSIZE, BlockSize) == -1)
	//~ {
	  //~ OUTPUT_TRACE_ERROR("SNDCTL_DSP_GETBLKSIZE ioctl error\n");
	//~ }
	
    /* Get the size of the buffer */
	 if((int)currentAlsaPcmFormat<5)
			sampleSz=2;
     else if ((int)currentAlsaPcmFormat<9)
          sampleSz=3;
     else
             sampleSz=4;

	 //printf("sample size is : %d\n",sampleSz);
	
	 *BlockSize = (int)currentPeriodSize * 1 * sampleSz /* 2 -> sample size */;

	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);	
		
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_GetOSpace(audio_buf_info *DSP_Info)
    \brief Get space.
    \param DSP_Info free space.
*/

 CRED_Errors_t OUTPUT_GetOSpace(audio_buf_info *DSP_Info)
{

	pthread_mutex_lock(&OutputBuffer.g_OutputMutex);
	
	if(ioctl(output_fd, SNDCTL_DSP_GETOSPACE, DSP_Info) == -1)
	{
	  OUTPUT_TRACE_ERROR("SNDCTL_DSP_GETOSPACE ioctl error\n");
	}
	
	pthread_mutex_unlock(&OutputBuffer.g_OutputMutex);
	
	return 0;
}

/*! \fn CRED_Errors_t OUTPUT_LGetFreeBufferSize()
    \brief Get space.
    \param DSP_Info free space.
*/

int OUTPUT_LGetFreeBufferSize()
{
	return (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.left_size);
}

/*! \fn CRED_Errors_t OUTPUT_RGetFreeBufferSize()
    \brief Get space.
    \param DSP_Info free space.
*/

int OUTPUT_RGetFreeBufferSize()
{
	return (OUTPUT_MAX_SIZE_TO_WRITE - OutputBuffer.right_size);
}

#endif
