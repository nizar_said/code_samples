/****************************************************************
 *                                                              *
 * Copyright (c) Nuvoton Technology Corp.  All rights reserved. *
 *                                                              *
 ****************************************************************/


/*#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/stat.h>*/

#include "AACRecorder_Agent.h"



// ==================================================
// Private function declaration
// ==================================================
void s_EncodeThread(void* encode);
void s_RecordThread(void* encode);

// ==================================================
// Plublic function implementation
// ==================================================

CRED_Errors_t Close_Audio_Record(AUDIO_Enc encode) 
{
	if (encode.s_i32DevDSP >= 0) 
	{
		close(encode.s_i32DevDSP);
		encode.s_i32DevDSP = -1;
	} 
	else
	{
		AUDIO_TRACE_ERROR( "Wrong dsp encode fd\n");
		return CRED_ERROR_BAD_FD;
	}
	return CRED_NO_ERROR;
} 
CRED_Errors_t s_OpenAudioRecDevice(int	i32SampleRate,int	i32ChannelNum,AUDIO_Enc *encode)
 {
	int rec_volume;
	struct stat statbuf;
	int len;
	int oss_format;
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	encode->s_i32DevDSP_BlockSize = 0;
	if(encode->input==micro) 
	{
		encode->s_i32DevDSP = open("/dev/dsp1", O_RDWR);
		if (encode->s_i32DevDSP < 0) 
		{
			AUDIO_TRACE_ERROR( "Failed to open /dev/dsp1 \n");
			return CRED_ERROR_BAD_FD;
		} 
		
		oss_format=AFMT_S16_LE;
		ioctl(encode->s_i32DevDSP, SNDCTL_DSP_SETFMT,&oss_format);
		fcntl(encode->s_i32DevDSP, F_SETFL, O_NONBLOCK);

		if (ioctl(encode->s_i32DevDSP, SNDCTL_DSP_SPEED, &i32SampleRate) < 0) 
		{
			Close_Audio_Record(*encode); 
			return CRED_ERROR_BAD_PARAMETER;
		} 

		ioctl(encode->s_i32DevDSP, SNDCTL_DSP_CHANNELS, &i32ChannelNum);
		ioctl(encode->s_i32DevDSP, SNDCTL_DSP_GETBLKSIZE, &encode->s_i32DevDSP_BlockSize);
		if ((encode->mixer_fd = open("/dev/mixer1", O_WRONLY)) == -1)  
		{
			AUDIO_TRACE_ERROR("open /dev/mixer1 error \n");
			return CRED_ERROR_BAD_FD;
		}
		rec_volume = (80<< 8) |80; //************
		if(ioctl(encode->mixer_fd , MIXER_WRITE(SOUND_MIXER_PCM), &rec_volume)==-1)	//******
		{
			AUDIO_TRACE_ERROR("Failed MIXER_WRITE to Set volume \n");
			return CRED_ERROR_DEVICE;
		}
		
	}
	else	
	{
		if (stat("./test.pcm", &statbuf) == -1) 
		{
			AUDIO_TRACE_ERROR("erreur file pcm \n");
		}
		len = statbuf.st_size;
		encode->s_i32DevDSP = open("./test.pcm", O_RDONLY);
		if (encode->s_i32DevDSP < 0) 
		{
			AUDIO_TRACE_ERROR("Can not open pcm file %s\n");
			return CRED_ERROR_BAD_PARAMETER;
		}
		encode->s_i32DevDSP_BlockSize = 0x800;		
		encode->s_i32no = (len/encode->s_i32DevDSP_BlockSize)+1; 
		int pos = lseek(encode->s_i32DevDSP, 0x2C, SEEK_SET);
		if ( pos == -1 )
		{
			AUDIO_TRACE_ERROR("seek position error\n");
			return CRED_ERROR_BAD_PARAMETER;
		}
	}
	return CRED_NO_ERROR;
} 
CRED_Errors_t AUDIO_Record(char* pczFileName,AUDIO_Enc *encode)
{	
	pthread_t	sRecordThread;
	pthread_t 	sEncodeThread;
	int	i32Index;

	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex); 
	if (!pczFileName|| encode->g_eAACRecorder_State != eAACRECORDER_STATE_STOPPED)
	{
		AUDIO_TRACE_ERROR("error stat or file name for record file\n");
		return CRED_ERROR_BAD_PARAMETER;
	} 
	
	encode->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING; 
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	encode->s_psAACFile = fopen(pczFileName, "wb"); 
	if (encode->s_psAACFile == NULL) 
	{
		AUDIO_TRACE_ERROR(" error FileName \n");
		return CRED_ERROR_BAD_PARAMETER;
		/*bResult = false;
		goto EndOfRecordFile;*/
	} 
	encode->s_i32PCMFrame_FullNum = 0;
	memset((void*)encode->s_ai32PCMFrame_PCMSize, 0, sizeof(encode->s_ai32PCMFrame_PCMSize));
	for (i32Index = 0; i32Index < PCMFRAME_BUFNUM; i32Index++) 
	{
		encode->s_apcPCMFrame_Buf[i32Index] = malloc(PCMFRAME_BUFSIZE);
		if (encode->s_apcPCMFrame_Buf[i32Index] == NULL)
		{
			AUDIO_TRACE_ERROR("error malloc() buffer for encode \n");
			return CRED_ERROR_BAD_PARAMETER;
			/*bResult = false;
			goto EndOfRecordFile;*/
		} 
	}
	
	encode->s_bRecordThread_Alive = true;
	if (pthread_create(&sRecordThread, NULL, (void*)s_RecordThread, (void *) encode) != 0) 
	{
		encode->s_bRecordThread_Alive = false;
		AUDIO_TRACE_ERROR("erreur Record thread \n");
		return CRED_ERROR_BAD_PARAMETER;
		//bResult = false;
		//goto EndOfRecordFile;
	} 
	
	do {
		struct timespec	sSleepTime = { 0, 0 };
		nanosleep(&sSleepTime, NULL);
	} while (encode->s_bRecordThread_Alive && encode->s_i32PCMFrame_FullNum < 1);

	if (encode->s_i32PCMFrame_FullNum > 0) 
	{
		encode->s_bEncodeThread_Alive = false;
		if (pthread_create(&sEncodeThread, NULL, (void*)s_EncodeThread,(void *) encode) != 0) 
		{
			AUDIO_TRACE_ERROR("erreur Encode thread \n");
			//bResult = false;
			return CRED_ERROR_BAD_PARAMETER;
		} 
	} 
	else 
	{
		return CRED_ERROR_BAD_PARAMETER;
		//bResult = false;
	} 

	return CRED_NO_ERROR;
/*EndOfRecordFile:
	if (bResult == false)
	return bResult;*/
} 
CRED_Errors_t AUDIO_Record_Stop(AUDIO_Enc *encode) 
{	
	int	i32Index;
	CRED_Errors_t err = CRED_NO_ERROR;
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->s_bRecordThread_Alive || encode->s_bEncodeThread_Alive) 
	{
		encode->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		do 
		{
			struct timespec	sSleepTime = {0,0};
			nanosleep(&sSleepTime, NULL);
		} while (encode->s_bRecordThread_Alive || encode->s_bEncodeThread_Alive);
	}

	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	memset((void*)encode->s_ai32PCMFrame_PCMSize, 0, sizeof(encode->s_ai32PCMFrame_PCMSize));
	for (i32Index = PCMFRAME_BUFNUM - 1; i32Index >= 0; i32Index--)
		if (encode->s_apcPCMFrame_Buf[i32Index]) {
			free((void*)(encode->s_apcPCMFrame_Buf[i32Index]));
			encode->s_apcPCMFrame_Buf[i32Index] = NULL;
		}

	if (encode->s_psAACFile)
	{
		fclose(encode->s_psAACFile);
		encode->s_psAACFile = NULL;
		//sync();
	} 

	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	encode->g_eAACRecorder_State = eAACRECORDER_STATE_STOPPED;
	AUDIO_TRACE_INFO("stopped Encode\n");
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	err=Close_Audio_Record(*encode); //**********///*****************///****
	
	return err;
} 
CRED_Errors_t AUDIO_Record_Pause(AUDIO_Enc *encode) 
{
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->g_eAACRecorder_State != eAACRECORDER_STATE_RECORDING)
	{
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it must be playing\n");
		return CRED_ERROR_AUDIO_ENCODE_PAUSE;
	} 

	encode->g_eAACRecorder_State = eAACRECORDER_STATE_TOPAUSE;
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);

	while (1) 
	{
		struct timespec	sSleepTime = { 0, 0 };
		nanosleep(&sSleepTime, NULL);

		pthread_mutex_lock(&encode->g_sRecorderStateMutex);
		if (encode->g_eAACRecorder_State == eAACRECORDER_STATE_PAUSED) 
		{
			pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
			break;
		} 
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	} 

	return CRED_NO_ERROR;
}
CRED_Errors_t AUDIO_Record_Resume(AUDIO_Enc *encode) 
{
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->g_eAACRecorder_State != eAACRECORDER_STATE_PAUSED) 
	{
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio encode state it must be paused\n");
		return CRED_ERROR_AUDIO_ENCODE_RESUME;
	} 

	encode->g_eAACRecorder_State = eAACRECORDER_STATE_TORESUME;
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);

	while (1)
	{
		
		struct timespec	sSleepTime = {0,0};
		nanosleep(&sSleepTime, NULL);

		pthread_mutex_lock(&encode->g_sRecorderStateMutex);
		if (encode->g_eAACRecorder_State == eAACRECORDER_STATE_RECORDING) 
		{
			pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
			break;
		} 
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	} 

	return CRED_NO_ERROR;
}
// ==================================================
// Private Function implementation
// ==================================================

void s_RecordThread(void* encode) {
	AUDIO_Enc *encode_th = (AUDIO_Enc *)encode;
	encode_th->s_bRecordThread_Alive = true;
	int  number=0;
	int		i32ReadBytes;
	struct timespec	sSleepTime = { 0, 0};
	int		s_i32PCMFrame_WriteIdx = 0;
	int value_ptr;
	CRED_Errors_t err = CRED_NO_ERROR;
	
	pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
	if (encode_th->g_eAACRecorder_State != eAACRECORDER_STATE_RECORDING) 
	{
	
		pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
		encode_th->s_bRecordThread_Alive = false;
		return;
	}
	
	pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex); 
	pthread_detach(pthread_self()); 
	
	err = s_OpenAudioRecDevice(AACRECORDER_SAMPLE_RATE, AACRECORDER_CHANNEL_NUM,encode_th);
	if (err != CRED_NO_ERROR)
	{
	AUDIO_TRACE_ERROR("Open Audio RecDevice failed %d\n",err);	
	}
	
	if (encode_th->s_i32DevDSP_BlockSize > 0) {
		
		char	*pcPCMBuf = malloc(encode_th->s_i32DevDSP_BlockSize);

		if (pcPCMBuf == NULL) {
			AUDIO_TRACE_ERROR("malloc() in Record thread \n");
			goto EndOfRecordThread;
		} // if

		unsigned int g_u32AACRecorder_RecFrameCnt = 0;
		while (1) 
		{
			
			pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOSTOP) 
			{
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				break;
			} 
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOPAUSE && encode_th->input == file) 
			{	
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_PAUSED;
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				continue;
			} 
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOPAUSE && encode_th->input == micro) 
			{	
				err = Close_Audio_Record(*encode_th);
				if (err != CRED_NO_ERROR)
				{
				AUDIO_TRACE_ERROR("CLose Audio RecDevice failed for pause encode %d\n",err);	
				}
				close(encode_th->mixer_fd);
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_PAUSED;
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				continue;
			} 

			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_PAUSED) 
			{
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				struct timespec	sSleepTime = { 0, 0 };
				nanosleep(&sSleepTime, NULL);
				continue;
			}
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TORESUME && encode_th->input == micro) 
			{		
			err = s_OpenAudioRecDevice(AACRECORDER_SAMPLE_RATE, AACRECORDER_CHANNEL_NUM,encode_th);
			if (err != CRED_NO_ERROR)
			{
			AUDIO_TRACE_ERROR("Open Audio RecDevice failed for resume encode %d\n",err);	
			}
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
			}
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TORESUME && encode_th->input == file) 
			{		
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
			}
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			int		i32ReadBytes = read(encode_th->s_i32DevDSP, pcPCMBuf, encode_th->s_i32DevDSP_BlockSize); 
			
			if(encode_th->input == file)
			{
			number ++;
				if (( number > encode_th->s_i32no) || (i32ReadBytes <= 0 ))
				{

					i32ReadBytes = 0; 
				do {
					nanosleep(&sSleepTime, NULL);
					}while (encode_th->s_i32PCMFrame_FullNum > 0);
			
               encode_th->s_bRecordThread_Alive = false;
			   encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
				}
			}
			else
			{
				if (i32ReadBytes <= 0 )
				{
			   i32ReadBytes = 0; 
				do {
					nanosleep(&sSleepTime, NULL);
					}while (encode_th->s_i32PCMFrame_FullNum > 0);
               encode_th->s_bRecordThread_Alive = false;
			   encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
				}
			}
			bool	bWarning = false;
		
			while (i32ReadBytes > 0) 
			{
				if (encode_th->s_i32PCMFrame_FullNum < PCMFRAME_BUFNUM-1) 
				{
			char	*pcPCMDst = (char*)(encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_WriteIdx]) + encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx],
					*pcPCMSrc = pcPCMBuf; 
					int	i32FillBytes = PCMFRAME_BUFSIZE - encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx];
					
					if (i32FillBytes > i32ReadBytes)
						i32FillBytes = i32ReadBytes;
					
					memcpy(pcPCMDst, pcPCMSrc, i32FillBytes);
					pcPCMDst += i32FillBytes;
					pcPCMSrc += i32FillBytes;

					encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx] += i32FillBytes;
					if (encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx] >= PCMFRAME_BUFSIZE)
					{
						g_u32AACRecorder_RecFrameCnt++;

						s_i32PCMFrame_WriteIdx++;
						if ( s_i32PCMFrame_WriteIdx == PCMFRAME_BUFNUM )
							s_i32PCMFrame_WriteIdx = 0;
			
						pcPCMDst = (char*)encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_WriteIdx]; 
						encode_th->s_i32PCMFrame_FullNum++;
					} 
					else {
					} 
					i32ReadBytes -= i32FillBytes;
				}
				else 
				{

					if (bWarning == false) 
					{
						bWarning = true;
					}
					
					struct timespec	sSleepTime = { 0, 0 };
					nanosleep(&sSleepTime, NULL);
				} 
			} 

			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
		} 
	} 
EndOfRecordThread:

	Close_Audio_Record(*encode_th);
	encode_th->s_bRecordThread_Alive = false;
	pthread_exit(&value_ptr);
	AUDIO_TRACE_INFO("Exit Record thread \n");
} 


void s_EncodeThread(void* encode) 
{
      
	AUDIO_Enc *encode_th = (AUDIO_Enc *)encode;
	encode_th->s_bEncodeThread_Alive = true;
	int		s_i32PCMFrame_ReadIdx = 0;
	pthread_detach(pthread_self()); 
	char	*pcEncFrameBuf = malloc(ENCFRAME_BUFSIZE);
	int value_ptr;
	
	if (pcEncFrameBuf == NULL) 
	{
		AUDIO_TRACE_ERROR("error malloc() for pcEncFrameBuf \n");
		goto EndOfEncode;		
	} 

	S_AACENC sEncoder;

	sEncoder.m_u32SampleRate = AACRECORDER_SAMPLE_RATE;
	sEncoder.m_u32ChannelNum = AACRECORDER_CHANNEL_NUM;
	sEncoder.m_u32BitRate = AACRECORDER_BIT_RATE * sEncoder.m_u32ChannelNum;
	sEncoder.m_u32Quality = AACRECORDER_QUALITY;
	sEncoder.m_bUseAdts = true;
	sEncoder.m_bUseMidSide = false;
	sEncoder.m_bUseTns = false;
	
	E_AACENC_ERROR	eAACEnc_Error = AACEnc_Initialize(&sEncoder);

	if (eAACEnc_Error != eAACENC_ERROR_NONE) 
	{
		AUDIO_TRACE_ERROR("AAC Recorder: Fail to initialize AAC Encoder \n", eAACEnc_Error);
		goto EndOfEncode;		
	} 
	
	unsigned int g_u32AACRecorder_EncFrameCnt = 0;

	while (1) {
		
		pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOSTOP)
		{
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			break;
		} 
		pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
		
		if (encode_th->s_i32PCMFrame_FullNum > 0) 
		{
			int	i32EncFrameSize;
			eAACEnc_Error = AACEnc_EncodeFrame((short*)encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_ReadIdx], pcEncFrameBuf, ENCFRAME_BUFSIZE, &i32EncFrameSize); // pcEncFrameBuf elements à ecrire
			if (eAACEnc_Error != eAACENC_ERROR_NONE) 
			{
				AUDIO_TRACE_ERROR("AAC Recorder: Fail to encode file\n", eAACEnc_Error);
				goto EndOfEncode;		
			} 

			if (fwrite(pcEncFrameBuf, 1, i32EncFrameSize, encode_th->s_psAACFile) != i32EncFrameSize) // write AACfile
			{
				AUDIO_TRACE_ERROR("fwrite() in AACFile \n");
				goto EndOfEncode;	
			} 
			g_u32AACRecorder_EncFrameCnt++;
			encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_ReadIdx] = 0;
			s_i32PCMFrame_ReadIdx++;
			if ( s_i32PCMFrame_ReadIdx == PCMFRAME_BUFNUM )
    			s_i32PCMFrame_ReadIdx = 0;

			/*if (s_i32PCMFrame_ReadIdx == 0)
				sync();*/
			encode_th->s_i32PCMFrame_FullNum--;
		} 
		else if (encode_th->s_bRecordThread_Alive) 
		{		
			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
		}
		else 
		{
 		    encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
			break;
		} 
	} 
    
EndOfEncode:
	 
	AACEnc_Finalize();
	if (pcEncFrameBuf) 
	{
		free(pcEncFrameBuf);
	}

	encode_th->s_bEncodeThread_Alive = false;
	pthread_exit(&value_ptr);
	AUDIO_TRACE_INFO("Exit Encode thread \n");
} 
