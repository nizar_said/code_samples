
#ifndef __TOOLS_HEADER__
#define  __TOOLS_HEADER__
#include "tools.h"
#endif


typedef enum
{
	play	 	= 0,
	stop		= 1,
	pause_ply   = 2,
	resume 		= 3,
	
} enum_play;
typedef enum
{
	play_rec   =  0,
	stop_rec   =  1,
	pause_rec  =  2,
	resume_rec =  3,
	
} enum_record;

typedef struct 
{
int p_mixer;
int p_dsp;
int seconds;
enum_record state_record;
enum_play state_play ;

} PCM;

void play_single(void *pcm);
void record(PCM *pcm);

//void record_to_pcm_adc(PCM *pcm);
//void void_play_pcm_single(PCM *pcm);
CRED_Errors_t close_audio_play_device(PCM pcm);
CRED_Errors_t open_audio_play_device(PCM *pcm);
CRED_Errors_t stop_play(PCM *pcm);
CRED_Errors_t stop_record(PCM *pcm);
CRED_Errors_t pause_play(PCM *pcm);
CRED_Errors_t pause_record(PCM *pcm);
CRED_Errors_t resume_play(PCM *pcm);
CRED_Errors_t resume_record(PCM *pcm);
