/* audio.c
 *
 *
 * Copyright (c)2008 Nuvoton technology corporation
 * http://www.nuvoton.com
 *
 * Audio playback demo application
 *
 */

/*#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/soundcard.h>
#include <sys/poll.h>
#include <pthread.h>
#include <math.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>*/
#include "PCM.h"

CRED_Errors_t close_audio_play_device(PCM pcm)
{

	if(pcm.p_dsp >0  && pcm.p_mixer > 0)
	{
	close(pcm.p_dsp);
	close(pcm.p_mixer);
	}
	else 
	{
		AUDIO_TRACE_ERROR( "Wrong dsp fd\n");
		return CRED_ERROR_BAD_FD;
	}
	
  return CRED_NO_ERROR;	
}

CRED_Errors_t open_audio_play_device(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pcm->p_dsp = open("/dev/dsp", O_RDWR);
	if( pcm->p_dsp < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open any dsp device\n");
		return CRED_ERROR_BAD_FD;
	}

	pcm->p_mixer = open("/dev/mixer", O_RDWR);
	if( pcm->p_mixer < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open any dsp device\n");
		return CRED_ERROR_BAD_FD;
	}
	return CRED_NO_ERROR;
}

CRED_Errors_t stop_play(PCM *pcm)
{
		if (pcm == NULL)
		{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
		}	
		if (pcm->state_play == play)
		{
		pcm->state_play=stop;
		}
		else
		{
		AUDIO_TRACE_ERROR("Failed to Stop play PCM \n");
		return CRED_ERROR_DEVICE;
		}
	return CRED_NO_ERROR;
	
}

CRED_Errors_t stop_record(PCM *pcm)
{	
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pcm->state_record == play_rec)
	{
	pcm->state_record = stop_rec;
	}
	else
	{
		AUDIO_TRACE_ERROR("Failed to Stop record PCM \n");
		return CRED_ERROR_DEVICE;
	}
	return CRED_NO_ERROR;
}
CRED_Errors_t pause_play(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pcm->state_play == play)
	{
		pcm->state_play = pause_ply;
	}
	else
	{
		AUDIO_TRACE_ERROR("Failed to pause play PCM \n");
		return CRED_ERROR_DEVICE;
	}
return CRED_NO_ERROR;
}
CRED_Errors_t pause_record(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pcm->state_record == play_rec)
	{
		pcm->state_record = pause_rec;
	}
	else
	{
		AUDIO_TRACE_ERROR("Failed to pause record PCM \n");
		return CRED_ERROR_DEVICE;
	}
	//else
	//printf("enable to do pause record \n");
return CRED_NO_ERROR;
}
CRED_Errors_t resume_play(PCM *pcm)
{	
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pcm->state_play == pause_ply)
	{
		pcm->state_play = resume;
	}
	else
	{
		AUDIO_TRACE_ERROR("Failed to resume play PCM \n");
		return CRED_ERROR_DEVICE;
	}
return CRED_NO_ERROR;
}	
CRED_Errors_t resume_record(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}	
	if (pcm->state_record == pause_rec)
	{
		pcm->state_record = resume_rec;
	}
	else
	{
		AUDIO_TRACE_ERROR("Failed to resume play PCM \n");
		return CRED_ERROR_DEVICE;
	}
	
return CRED_NO_ERROR;
}
CRED_Errors_t void_play_pcm_single(PCM *pcm)	
{	
	pthread_t 	thread_play_pcm_single;
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pthread_create(&thread_play_pcm_single, NULL, (void*)play_single, (void *)pcm)< 0) 
	{ 
		AUDIO_TRACE_ERROR(" erreur create PLay thread");
		return CRED_ERROR_BAD_PARAMETER;
	}
	return CRED_NO_ERROR;
}
CRED_Errors_t record_to_pcm_adc(PCM *pcm)	
{	
	pthread_t 	thread_record;
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (pthread_create(&thread_record, NULL, (void*)record, (void *)pcm)< 0) 
	{ 
		AUDIO_TRACE_ERROR(" erreur create record thread");
		return CRED_ERROR_BAD_PARAMETER;
	}
	return CRED_NO_ERROR;
}


void play_single(void *pcm)	
{	
	PCM *pcm_th = (PCM *)pcm;
	int data, oss_format, channels, sample_rate;	
	//int i;
	char *buffer;
	int value;
	int frag;
	FILE *fd;

	int samplerate =16000;
	pcm_th->state_play = play;
	pthread_detach(pthread_self()); 
	fd = fopen("./rec.pcm", "r+");
    if(fd == NULL)
    {
    	AUDIO_TRACE_ERROR("open rec.pcm error!\n");
    	return;
    }
    open_audio_play_device(pcm_th);
	data = 0x5050;
	oss_format=AFMT_S16_LE;
	
	sample_rate = samplerate;
	channels = 1;
	ioctl(pcm_th->p_dsp, SNDCTL_DSP_SETFMT, &oss_format);
	ioctl(pcm_th->p_mixer, MIXER_WRITE(SOUND_MIXER_PCM), &data); 
	ioctl(pcm_th->p_dsp, SNDCTL_DSP_SPEED, &sample_rate);
	ioctl(pcm_th->p_dsp, SNDCTL_DSP_CHANNELS, &channels);
			
	
	ioctl(pcm_th->p_dsp, SNDCTL_DSP_GETBLKSIZE, &frag);
	buffer = (char *)malloc(frag);
	
	fread(buffer, 1, frag, fd); // read & save in buffer

			
	while((!feof(fd)))
	{		
		if(pcm_th->state_play == pause_ply)
		{
			continue;
		}
		if(pcm_th->state_play == resume)
		{
			pcm_th->state_play = play;
		}
		if(pcm_th->state_play == stop)
			break;
		
		audio_buf_info info;			
		do
		{			
			ioctl(pcm_th->p_dsp , SNDCTL_DSP_GETOSPACE , &info); 			
			usleep(100);
		}while(info.bytes < frag);
		
		fd_set writefds;
		struct timeval tv;
		tv.tv_sec       = 0;
		tv.tv_usec      = 0;
		FD_ZERO( &writefds );
		FD_SET( pcm_th->p_dsp , &writefds ); 
		tv.tv_sec       = 0;
		tv.tv_usec      = 0;
		
		select( pcm_th->p_dsp + 1 , NULL , &writefds , NULL, &tv ); //
		if( FD_ISSET( pcm_th->p_dsp, &writefds ))
		{	
			write(pcm_th->p_dsp, buffer, frag); 
			fread(buffer, 1, frag, fd);  
		}	
		usleep(100);
		
	}
	int bytes;
	ioctl(pcm_th->p_dsp,SNDCTL_DSP_GETODELAY,&bytes); //This ioctl call tells how long time it's going to take before the next sample to be written gets played by the hardware
	int delay = bytes / (sample_rate * 2 * channels);	
	sleep(delay);
	
	//printf("Stop Play %d \n", pcm_th->state_play); //************
	fclose(fd);
	free(buffer);
	close_audio_play_device(*pcm_th);
	pthread_exit(&value);
}

void record(PCM *pcm)
{
	PCM *pcm_th = (PCM *)pcm;
	int samplerate =16000;
	int audio_fd, music_fd, mixer_fd, count;
	//int mode;
	//char para;
	int format;
	int channels = 1;   
	//int recsrc;
	int totalbyte= samplerate * channels * 2 * pcm_th->seconds;
	int total = 0;
	int32_t speed;
	int value;
	//int buf;
	static volatile int rec_volume; 

	signed short applicbuf[2048];
	pthread_detach(pthread_self()); 
	printf("Sample rate = %d\n", samplerate);
	printf("totalbyte = %d\n", totalbyte);

	pcm_th->state_record =play_rec;
	
	if ((audio_fd = open("/dev/dsp1",O_RDONLY,0)) == -1) 
	{
		AUDIO_TRACE_ERROR("/dev/dsp1");
		return;
	}
	printf("open dsp1 successful\n");
	
	format = AFMT_S16_LE;
	if (ioctl(audio_fd,SNDCTL_DSP_SETFMT, &format) == -1) 
	{
		AUDIO_TRACE_ERROR("SNDCTL_DSP_SETFMT");
		return;	
	}	
	if (ioctl(audio_fd, SNDCTL_DSP_SPEED, &samplerate) == -1) 
	{
		AUDIO_TRACE_ERROR("SNDCTL_DSP_SPEED");
		return;
	} 
	else 
		printf("Actual Speed : %d \n",speed);
	
	if ((mixer_fd = open("/dev/mixer1", O_WRONLY)) == -1) 
    {
      	AUDIO_TRACE_ERROR("open /dev/mixer1 error");
        return;
    }
	//printf("open mixer successful\n");
	rec_volume = (100 << 8) | 100;	
	ioctl(mixer_fd , MIXER_WRITE(SOUND_MIXER_PCM), &rec_volume);	
	//printf("set mic volume = 0x%x   !!!\n",rec_volume);	
	
	if ((music_fd = open("./rec.pcm", O_WRONLY | O_CREAT | O_TRUNC, 0)) == -1) 
	{
        AUDIO_TRACE_ERROR("rec.pcm");
        return;
    }      		
	
    while ((total != totalbyte)) 
    {
		if(pcm_th->state_record == pause_rec)
		{
			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
			//printf("status pause record %d \n",pcm_th->state_record);
			continue;
		}
		else if(pcm_th->state_record == resume_rec)
		{
			//printf("status resume record %d \n",pcm_th->state_record);
			pcm_th->state_record = play_rec;
		}
		else if(pcm_th->state_record == stop_rec)
			break;
		
		if(totalbyte - total >= 2048)
			count = 2048;
		else 
			count = totalbyte - total;
		
		read(audio_fd, applicbuf, count);  
		write(music_fd, applicbuf, count); 
		total += count;  
			
	}
	close(mixer_fd);
	close(audio_fd);
	close(music_fd);
	pthread_exit(&value);
}


	
