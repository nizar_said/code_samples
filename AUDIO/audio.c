/* audio.c
 *
 *
 * Copyright (c)2015 comelit R&D Tunisia
 *
 */
 
#include "notification_cred.h"
#include "middleware_cred.h"
#include "multimedia_cred.h"
#include "output_cred.h"

#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096
#define PCM_DEVICE "card2"			 											///< The name of record audio device.
#define PLAY_ALSA_PERIOD_SIZE       1024                             			///< Number of samples for period play (setted into alsa setup)
#define CAPTURE_ALSA_PERIOD_SIZE       160                            			///< Number of samples for period play (setted into alsa setup)
#define CAPTURE_ALSA_MS_DURATION        320
#define CAPTURE_BUFF_DURATION_US        (CAPTURE_ALSA_MS_DURATION*1000)			///< ALSA mic us duration

static struct Multimedia_event_Str Multimedia_event;
static struct Ns_event_Str MsEvent;
CRED_MW_Errors_t (*audio_ms_notify)(struct Ns_event_Str AsEvent);
static unsigned int volume_tmp = 0;			
static unsigned int decode_status = 0;	
extern CRED_output_buffer_t OutputBuffer;
pthread_mutex_t	audio_thread_mutex;
pthread_mutex_t	audio_stop_mutex;
pthread_mutex_t	audio_play_mutex;
static int pcm_play_status = 0;
static int PlaybackThreadStatus =0;
static  snd_pcm_format_t AlsaPcmFormat;          								///< audio pcm format
static  snd_pcm_t *captureHandle=NULL;               							///< Handle pointer for audio capture hardware.
static snd_pcm_uframes_t  audio_recording_frames;
static unsigned  int leftChannelVolume=0;										///< left channel audio volume.
static  unsigned int rightChannelVolume=0;		 								///< right channel audio volume.
SwrContext *swr;   

/**
 * AUDIO Structure 
 */
typedef struct 
{
	 AVCodec *codec;
	 AVCodecContext *c;
	 AVPacket *pkt;
	 AVFrame *decoded_frame ;
	 AVCodecParserContext *parser;
     uint8_t *data;
     size_t   data_size;
}AACDecoder_Params_Str;

/*! \fn CRED_Errors_t audio_aac_set_info(AUDIO_Enc *encode)
    \brief Set audio parameter(number frames recorded,total Encoded FrameSize,
    duration and bit rate) into file.
    \param AUDIO_Enc *encode a structure that contains all 
			the inputs/outputs parameters to encode audio.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t audio_aac_set_info(AUDIO_Enc *encode)
{
	double duration;
	double audio_duration;
	
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	duration = (encode->total_EncFrameSize * 8) /AACRECORDER_BIT_RATE;
	audio_duration = floor(duration);
	if(duration - audio_duration != 0)
	{
		audio_duration++ ;
	}
	
	FILE* audio_info_fd = fopen(encode->file_info_audio,"w");
	if (audio_info_fd == NULL)
	{
		AUDIO_TRACE_ERROR("Open audio file info failed \n");
		return CRED_ERROR_BAD_FD;	
	}
	
	fprintf(audio_info_fd,"%d\n",encode->total_frame_rec);
	fprintf(audio_info_fd,"%lu\n",encode->total_EncFrameSize);
	fprintf(audio_info_fd,"%d\n",(unsigned int) AACRECORDER_BIT_RATE);
	fprintf(audio_info_fd,"%u\n",(unsigned int) audio_duration);
	fclose(audio_info_fd);	
	return CRED_NO_ERROR;
	
}

/*! \fn CRED_Errors_t audio_aac_get_info(AUDIO_Enc *encode)
    \brief Get audio parameter(number frames recorded,total Encoded FrameSize,
    duration and bit rate) from file and save these parameters into audio->param_file struct.
    \param AUDIO_Enc *encode a structure that contains all 
			the inputs/outputs parameters to encode audio.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t audio_aac_get_info(AUDIO *audio)
{
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio decode to get info\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	audio->param_file.total_frame = 0 ;
	audio->param_file.size_file_rec = 0;
	audio->param_file.bit_rate_rec = 0;
	audio->param_file.duration_rec = 0;
	
	FILE* audio_info_fd = fopen(audio->file_info,"r");
	if (audio_info_fd == NULL)
	{
		
		AUDIO_TRACE_ERROR("Open audio file info aac failed \n");
		return CRED_ERROR_BAD_FD;	
	}
	fscanf(audio_info_fd,"%6d\n",&audio->param_file.total_frame);
	fscanf(audio_info_fd,"%6lu\n",&audio->param_file.size_file_rec);
	fscanf(audio_info_fd,"%6u\n",&audio->param_file.bit_rate_rec);
	fscanf(audio_info_fd,"%6u\n",&audio->param_file.duration_rec);
	fclose(audio_info_fd);
	
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t pcm_set_info(PCM *pcm)
    \brief Set audio parameter(totalbyte,duration and bit rate) into file.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs parameters to record pcm.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t pcm_set_info(PCM *pcm)
{
	double pcm_duration;
	double audio_pcm_duration;
	
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pcm_duration = (pcm->totalbyte * 8) /pcm_bit_rate;
	audio_pcm_duration = floor(pcm_duration);
	if(pcm_duration - audio_pcm_duration != 0)
	{
		audio_pcm_duration++ ;
	}
	
	FILE* pcm_info_fd = fopen(pcm->file_info_pcm,"w");
	if (pcm_info_fd == NULL)
	{
		AUDIO_TRACE_ERROR("Open pcm file info failed \n");
		return CRED_ERROR_BAD_FD;	
	}
	
	fprintf(pcm_info_fd,"%lu\n",pcm->totalbyte);
	fprintf(pcm_info_fd,"%d\n",(unsigned int) pcm_bit_rate);
	fprintf(pcm_info_fd,"%u\n",(unsigned int) audio_pcm_duration);
	fclose(pcm_info_fd);	
	return CRED_NO_ERROR;
	
}

/*! \fn CRED_Errors_t pcm_get_info(PCM *pcm)
    \brief Get audio parameter(totalbyte,duration and bit rate) 
    from file and save these parameters into pcm->param_file_pcm struct.
    \param PCM *pcm a structure that contains all the inputs/outputs parameters to record PCM.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t pcm_get_info(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pcm->param_file_pcm.size_file_pcm = 0;
	pcm->param_file_pcm.bit_rate_pcm = 0;
	pcm->param_file_pcm.pcm_duration_rec = 0;
	
	FILE* pcm_info_fd = fopen(pcm->file_info_pcm,"r");
	if (pcm_info_fd == NULL)
	{
		AUDIO_TRACE_ERROR("Open audio file pcm info failed \n");
		return CRED_ERROR_BAD_FD;	
	}
	
	fscanf(pcm_info_fd,"%16lu\n",&pcm->param_file_pcm.size_file_pcm);
	fscanf(pcm_info_fd,"%16u\n",&pcm->param_file_pcm.bit_rate_pcm);
	fscanf(pcm_info_fd,"%16u\n",&pcm->param_file_pcm.pcm_duration_rec);
	fclose(pcm_info_fd);
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t close_audio_play_device(PCM pcm)
    \brief Close audio device : Close Dsp and mixer after play audio pcm.
    \param PCM pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t close_audio_play_device(PCM pcm)
{

	if(pcm.p_dsp < 0)
	{
		AUDIO_TRACE_ERROR( "Wrong dsp fd\n");
		return CRED_ERROR_BAD_FD;
	}
	
	if(pcm.p_mixer < 0)
	{
		AUDIO_TRACE_ERROR( "Wrong mixer fd\n");
		return CRED_ERROR_BAD_FD;
	}

	close(pcm.p_dsp);
	close(pcm.p_mixer);

  return CRED_NO_ERROR;	
}

/*! \fn CRED_Errors_t close_audio_record_device(PCM pcm)
    \brief close_audio_record_device: Close Dsp1 and mixer1 after record audio pcm.
    \param PCM pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/  

CRED_Errors_t close_audio_record_device(PCM pcm)
{
	if(pcm.dsp_rec_pcm < 0)
	{
		AUDIO_TRACE_ERROR( "Wrong dsp fd\n");
		return CRED_ERROR_BAD_FD;
	}
	
	if(pcm.mixer_rec_pcm < 0)
	{
		AUDIO_TRACE_ERROR( "Wrong mixer fd\n");
		return CRED_ERROR_BAD_FD;
	}

	//~ close(pcm.dsp_rec_pcm);
	close(pcm.mixer_rec_pcm);

  return CRED_NO_ERROR;	
}

/*! \fn CRED_Errors_t open_audio_play_device(PCM *pcm)
    \brief Open Dsp and mixer to play audio pcm.
    \param PCM *pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t open_audio_play_device(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pcm->p_dsp = open("/dev/dsp", O_RDWR);
	if( pcm->p_dsp < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open any dsp device\n");
		return CRED_ERROR_BAD_FD;
	}

	pcm->p_mixer = open("/dev/mixer", O_RDWR);
	if( pcm->p_mixer < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open mixer device\n");
		return CRED_ERROR_BAD_FD;
	}
	
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t open_audio_record_device(PCM *pcm)
    \brief Open Dsp1 and mixer1 to record audio pcm.
    \param PCM *pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t open_audio_record_device(PCM *pcm)
{
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	//~ pcm->dsp_rec_pcm = open("/dev/dsp1", O_RDWR);
	if(OUTPUT_GetDspFd(&pcm->dsp_rec_pcm)==CRED_ERROR_BAD_FD)
	//~ if( pcm->dsp_rec_pcm < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open any dsp device\n");
		return CRED_ERROR_BAD_FD;
	}

	pcm->mixer_rec_pcm = open("/dev/mixer", O_RDWR);
	if(pcm->mixer_rec_pcm < 0 )
	{
		AUDIO_TRACE_ERROR( "Failed to open mixer device\n");
		return CRED_ERROR_BAD_FD;
	}
	
	return CRED_NO_ERROR;
}
/*! \fn CRED_Errors_t stop_play_pcm(PCM *pcm)
    \brief stop_play_pcm :Terminate play pcm process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t stop_play_pcm(PCM *pcm)
{
	struct timespec	sSleepTime = { 0, 0 };
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	if(pcm->audio_output_type == PLAY_ON_LEFT)
	{
		OUTPUT_left_Term();
	}else if(pcm->audio_output_type == PLAY_ON_RIGHT)
	{
		OUTPUT_right_Term();
	}else
	{
		AUDIO_TRACE_ERROR("audio output type error\n");
	}

	if(ioctl(&pcm->p_dsp, SNDCTL_DSP_SYNC, NULL) == -1)
	{
	  AUDIO_TRACE_ERROR("SNDCTL_DSP_SYNC ioctl error\n");
	}

	pthread_mutex_lock(&pcm->pcm_play_state_mutex);
	if(pcm->state_play == stop)
	{
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
		AUDIO_TRACE_ERROR( "Bad stop_record_pcm state : it is stopped\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
	if (pcm->play_pcm_thread)
	{
		pthread_mutex_lock(&pcm->pcm_play_state_mutex);
		pcm->state_play = tostop;
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
		do
		{
			nanosleep(&sSleepTime, NULL);
		} while (pcm->play_pcm_thread);
	}

	pthread_mutex_lock(&pcm->pcm_play_state_mutex);
	pcm->state_play = stop;
	pthread_mutex_unlock(&pcm->pcm_play_state_mutex);

	return CRED_NO_ERROR;
	
}

/*! \fn CRED_Errors_t stop_record_pcm(PCM *pcm)
    \brief stop_record_pcm :Terminate record process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t stop_record_pcm(PCM *pcm)
{	
	struct timespec	sSleepTime = { 0, 0 };
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&pcm->pcm_rec_state_mutex);
	if(pcm->state_record == stop_rec)
	{
		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
		AUDIO_TRACE_ERROR( "Bad stop_record_pcm state : it is stopped\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
	if (pcm->record_pcm_thread)
	{
		pthread_mutex_lock(&pcm->pcm_rec_state_mutex);
		pcm->state_record = tostop_rec;
		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
		do
		{
			nanosleep(&sSleepTime, NULL);
		} while (pcm->record_pcm_thread);
	}

	pthread_mutex_lock(&pcm->pcm_rec_state_mutex);
	pcm->state_record = stop_rec;
	pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);

	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t pause_play_pcm(PCM *pcm)
    \brief pause_play_pcm :Pause play pcm process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t pause_play_pcm(PCM *pcm)
{
	struct timespec	sSleepTime = { 0, 0 };
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pthread_mutex_lock(&pcm->pcm_play_state_mutex);
	if (pcm->state_play != play)
	{
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
		AUDIO_TRACE_ERROR( "Bad audio state pause pcm it must be playing\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pcm->state_play = topause;
	pthread_mutex_unlock(&pcm->pcm_play_state_mutex);

	while (1)
	{
		nanosleep(&sSleepTime, NULL);
		pthread_mutex_lock(&pcm->pcm_play_state_mutex);

		if (pcm->state_play == pause_play)
		{
			pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
			break;
		}
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
	}

	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t pause_record_pcm(PCM *pcm)
    \brief pause_record_pcm :Pause record pcm process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t pause_record_pcm(PCM *pcm)
{
	struct timespec	sSleepTime = { 0, 0 };
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pthread_mutex_lock(&pcm->pcm_rec_state_mutex);
	if (pcm->state_record != play_rec)
	{
		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
		AUDIO_TRACE_ERROR( "Bad audio state pause pcm it must be playing\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pcm->state_record = topause_rec;
	pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);

	while (1)
	{
		nanosleep(&sSleepTime, NULL);
		pthread_mutex_lock(&pcm->pcm_rec_state_mutex);

		if (pcm->state_record == pause_rec)
		{
			
			pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
			break;
		}

		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
	}

	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t resume_play_pcm(PCM *pcm)
    \brief resume_play_pcm :Resume play pcm process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t resume_play_pcm(PCM *pcm)
{	
	struct timespec	sSleepTime = {0,0};

	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}	
	pthread_mutex_lock(&pcm->pcm_play_state_mutex);

	if (pcm->state_play != pause_play)
	{
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
		AUDIO_TRACE_ERROR( "Bad audio state resume pcm it must be paused\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pcm->state_play = toresume;
	pthread_mutex_unlock(&pcm->pcm_play_state_mutex);

	while (1)
	{
		nanosleep(&sSleepTime, NULL);
		pthread_mutex_lock(&pcm->pcm_play_state_mutex);
		if (pcm->state_play == play)
		{
			pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
			break;
		}
		pthread_mutex_unlock(&pcm->pcm_play_state_mutex);
	}
	
	return CRED_NO_ERROR;
}	

/*! \fn CRED_Errors_t resume_play_pcm(PCM *pcm)
    \brief resume_record_pcm :Resume record pcm process.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t resume_record_pcm(PCM *pcm)
{
	struct timespec	sSleepTime = { 0, 0 };

	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}	
	pthread_mutex_lock(&pcm->pcm_rec_state_mutex);

	if (pcm->state_record != pause_rec)
	{
		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
		AUDIO_TRACE_ERROR( "Bad audio state resume pcm it must be paused\n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	pcm->state_record = toresume_rec;
	pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);

	while (1)
	{
		nanosleep(&sSleepTime, NULL);
		pthread_mutex_lock(&pcm->pcm_rec_state_mutex);
		if (pcm->state_record == play_rec)
		{
			pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
			break;
		}
		pthread_mutex_unlock(&pcm->pcm_rec_state_mutex);
	}
	
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t void_play_pcm_single(PCM *pcm)
    \brief launch the play pcm thread.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t void_play_pcm_single(PCM *pcm)	
{	
	pthread_t 	thread_play_pcm_single;
	pthread_mutex_lock(&audio_thread_mutex);
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		pthread_mutex_unlock(&audio_thread_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	if (CRED_TOOLS_CreateTask("thread_play_pcm_single", &thread_play_pcm_single, 0x2000,play_single, 99, SCHED_FIFO,(void *)pcm) < 0) 
	{ 
		AUDIO_TRACE_ERROR(" erreur create PLay thread");
		pthread_mutex_unlock(&audio_thread_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_unlock(&audio_thread_mutex);
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t record_to_pcm_adc(PCM *pcm)
    \brief launch the record pcm thread.
    \param PCM *pcm a structure that contains all 
			the inputs/outputs PCM parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t record_to_pcm_adc(PCM *pcm)	
{	
	pthread_t 	thread_record;
	if (pcm == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct pcm\n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	if (pcm->state_record != stop_rec)
	{
		AUDIO_TRACE_ERROR("erreur record thread pcm state to begin record \n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	if (CRED_TOOLS_CreateTask("thread_record", &thread_record, 0x2000,record, 99, SCHED_FIFO,(void *)pcm) < 0) 
	{ 
		AUDIO_TRACE_ERROR(" erreur create record thread");
		return CRED_ERROR_BAD_PARAMETER;
	}
	return CRED_NO_ERROR;
}
int audio_DeleteFSystemCache(FILE* __stream)
{
	int fd;
	
	if(__stream == NULL)
	{
		IPSTREAMING_TRACE_ERROR("null pointer\n");
		return -1; 
	}
	fd = fileno(__stream);
	if(fd == -1)
	{
		IPSTREAMING_TRACE_ERROR("fileno\n");
		return -1; 
	}
	fsync(fd);
	if(posix_fadvise(fd, 0, 0, POSIX_FADV_DONTNEED) == -1)
	{
		IPSTREAMING_TRACE_ERROR("posix_fadvise");
		close(fd);
		return -1;
	}
	//close(fd);
	return 0; 
}
/*! \fn void play_single(void *pcm)	
    \brief Thread to play pcm .
    \param void *pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval void:any value to return.
*/
void play_single(void *pcm)	
{	
	CRED_Errors_t err = CRED_NO_ERROR;
	PCM *pcm_th = (PCM *)pcm;
	int data, oss_format, channels, sample_rate;	
	int i;
	char *buffer;
	FILE *fd;
	int frag, delay,read_data;
	int value_ptr;
	pcm_play_status = 1;
	GsmSelectEnableSpeaker();

	/* Just For Test */
	AUDIO_Set_Right_Volume(20);
	AUDIO_Set_Left_Volume(20);
	//~ pcm_th->play_continious=CRED_PLAY_CONTINOUS_ON;
	
	pthread_detach(pthread_self());
	pcm_th->state_play = play;
	pcm_th->play_pcm_thread = true;
	fd = fopen(pcm_th->file_path_play, "r+");
    if(fd == NULL)
    {
    	AUDIO_TRACE_ERROR("open pcm file error!\n");
    	goto exit;
    }

	buffer = (char *)malloc(OUTPUT_MAX_SIZE_TO_WRITE);
	if (buffer == NULL)
	{
		AUDIO_TRACE_ERROR("error malloc() buffer for play pcm \n");
		fclose(fd);
		goto exit;
	}
	memset(buffer,0,OUTPUT_MAX_SIZE_TO_WRITE);
	AUDIO_TRACE_INFO("Start play file !\n");
	while(1)
	{		
		if(feof(fd))
		{
			if(pcm_th->play_continious == CRED_PLAY_CONTINOUS_ON)
			{
				fseek(fd,0, SEEK_SET);
			}else if(pcm_th->play_continious == CRED_PLAY_CONTINOUS_OFF)
			{
				goto end;
			}
		}

		if(pcm_th->audio_output_type == PLAY_ON_RIGHT)
		{
			 frag = OUTPUT_RGetFreeBufferSize();
		}else if(pcm_th->audio_output_type == PLAY_ON_LEFT)
		{
			 frag = OUTPUT_LGetFreeBufferSize();
		}else
		{
			AUDIO_TRACE_ERROR("unvalid play choice \n");
		}

		if(frag == 0)
		{
			usleep(10000);
			continue;
		}
		memset(buffer,0,OUTPUT_MAX_SIZE_TO_WRITE);
		read_data = fread(buffer, 1, frag, fd);
		if(read_data%4)
		{
			read_data+= (4-(read_data%4));
		}
		if(read_data<=0)
		{
			if(pcm_th->play_continious == CRED_PLAY_CONTINOUS_ON)
			{
				fseek(fd,0, SEEK_SET);
			}else if(pcm_th->play_continious == CRED_PLAY_CONTINOUS_OFF)
			{
				goto end;
			}
		} 

		pthread_mutex_lock(&pcm_th->pcm_play_state_mutex);
    	if (pcm_th->state_play == tostop)
    	{
    		AUDIO_TRACE_INFO("stop \n");
    		pthread_mutex_unlock(&pcm_th->pcm_play_state_mutex);
    		break;
    	}
    	if (pcm_th->state_play == topause)
    	{
    		AUDIO_TRACE_INFO("pause \n");
    		pcm_th->state_play = pause_play;
    		pthread_mutex_unlock(&pcm_th->pcm_play_state_mutex);
    		usleep(10000);
    		continue;
		}
    	if (pcm_th->state_play == pause_play)
    	{
    		AUDIO_TRACE_INFO("play \n");
    		pthread_mutex_unlock(&pcm_th->pcm_play_state_mutex);
    		 usleep(10000);

    		continue;
    	}
    	if (pcm_th->state_play == toresume)
    	{
			AUDIO_TRACE_INFO("resume \n");
			pcm_th->state_play = play;
    	}
    	pthread_mutex_unlock(&pcm_th->pcm_play_state_mutex);

		if(pcm_th->audio_output_type == PLAY_ON_RIGHT)
		{
			OUTPUT_RWrite(buffer, read_data);
		}else if(pcm_th->audio_output_type == PLAY_ON_LEFT)
		{
			OUTPUT_LWrite(buffer, read_data);
		}else
		{
			AUDIO_TRACE_ERROR("unvalid play choice \n");
		}

		//audio_DeleteFSystemCache(fd);
		usleep(200000);
	}

	end :
		audio_DeleteFSystemCache(fd);
		fclose(fd);
		free(buffer);

	exit :
		pcm_th->play_pcm_thread = false;
		pcm_th->state_play = stop;
		pcm_play_status = 0;
		pthread_exit(&value_ptr);

}

/*! \fn void record(void *pcm)	
    \brief Thread to record PCM .
    \param void *pcm a structure that contains all the inputs/outputs PCM parameters.
    \retval void:any value to return.
*/
void record(PCM *pcm)
{
	PCM *pcm_th = (PCM *)pcm;
	CRED_Errors_t err;
	int samplerate =16000;
	int count; 
	int mode;
	char para;
	int format;
	int channels = 1;   
	int speed;
	int value;
	int buf;
	FILE *fd;
	char *buffer;
    int size,ret;
	static volatile int rec_volume; 
	signed short applicbuf[2048];
	struct timespec	sSleepTime = { 0, 0 };
	pthread_detach(pthread_self());
	pcm_th->record_pcm_thread = true;
	pcm_th->state_record = play_rec;
	pcm_th->totalbyte = 0;
	
     		
	fd = fopen(pcm_th->file_name_rec, "w");
    if(fd == NULL)
    {
    	AUDIO_TRACE_ERROR("open pcm file error!\n");
    	goto exit;
    }
	        
    /* Initialize record hardware*/
	err = s_OpenAudioRecDevice(16000, 1,&size);
	printf(" recording block size is : %d\n",size);
	if (err != CRED_NO_ERROR)
	{
		AUDIO_TRACE_ERROR("Open Audio RecDevice failed %d\n",err);	
	}
    /* Prepare audio file header */
	buffer = (char *) malloc(size);
   

    while (1) 
    {
		
		pthread_mutex_lock(&pcm_th->pcm_rec_state_mutex);
    	if (pcm_th->state_record == tostop_rec)
    	{
    		pthread_mutex_unlock(&pcm_th->pcm_rec_state_mutex);
    		break;
    	}
    	
    	if (pcm_th->state_record == topause_rec)
    	{
    		close(pcm_th->mixer_rec_pcm);
    		pcm_th->state_record = pause_rec;
    		pthread_mutex_unlock(&pcm_th->pcm_rec_state_mutex);
    		nanosleep(&sSleepTime, NULL);
    		continue;
		}
		
    	if (pcm_th->state_record == pause_rec)
    	{
    		pthread_mutex_unlock(&pcm_th->pcm_rec_state_mutex);
    		continue;
    	}
    	
    	if (pcm_th->state_record == toresume_rec)
    	{
			//~ if ((pcm_th->mixer_rec_pcm = open("/dev/mixer1", O_WRONLY)) < 0) 
			//~ {
				//~ AUDIO_TRACE_ERROR("open /dev/mixer1 error \n");
			//~ }
			pcm_th->state_record = play_rec;
    	}
    	pthread_mutex_unlock(&pcm_th->pcm_rec_state_mutex);
    
    
    
		if(pcm_th->pcm_totalbyte_file - pcm_th->totalbyte >= size)
			count = size;
		else 
			count =  pcm_th->pcm_totalbyte_file - pcm_th->totalbyte;
		
		pcm_th->totalbyte = pcm_th->totalbyte + count; 
    	
		ret = snd_pcm_readi(captureHandle, buffer, CAPTURE_ALSA_PERIOD_SIZE);
		if (ret == -EPIPE) {
		  /* EPIPE means overrun */
		  fprintf(stderr, "overrun occurred\n");
		  snd_pcm_prepare(captureHandle);
		} else if (ret < 0) {
		  fprintf(stderr,
				  "error from read: %s\n",
				  snd_strerror(ret));
		} else if (ret != (int)CAPTURE_ALSA_PERIOD_SIZE) {
		  fprintf(stderr, "short read, read %d frames\n", ret);
		}
		/* write recorded msg to file*/
		
		ret = fwrite(buffer,count,1,fd);
 
		//~ if (write(music_fd, applicbuf, count) == -1)
		//~ {
			//~ AUDIO_TRACE_ERROR("PCM Record: Fail to write into file \n"); 
		//~ }
		
		
		if (pcm_th->totalbyte >= pcm_th->pcm_totalbyte_file) 
		{
			AUDIO_TRACE_ERROR("PCM Record: Fail to continue record the maximum file size is reached \n"); 
			break; 
		}
				
	}
	free(buffer);
	fclose(fd);
    err = AUDIO_Record_Release(NULL);
	if (err != CRED_NO_ERROR) 
	{
		AUDIO_TRACE_ERROR("failed to release allocated buffers for record %d\n",err);	
	}
    captureHandle=NULL;
	
	exit :
	pcm_th->record_pcm_thread = false;
	pcm_th->state_record = stop_rec;
	pthread_exit(&value);
}

unsigned int AACDecoder_Read_Callback(void	*pFile,void	*pBuffer, unsigned int	u32Length) 
{
	if (pFile == NULL || pBuffer==NULL)
	{
		AUDIO_TRACE_INFO("error Read Callback\n");
	}
	
	return fread(pBuffer, 1, u32Length, (FILE*)pFile);
} 

unsigned int AACDecoder_Seek_Callback(void	*pFile,unsigned long long	u64Position) 
{
	const unsigned long	u32CurPosition = ftell((FILE*)pFile);
	
	if (pFile == NULL)
	{
		AUDIO_TRACE_INFO("error Seek Callback\n");
	}
	
	if (u64Position < u32CurPosition) 
	{
		AUDIO_TRACE_INFO("Backward seek to %lld from %ld bytes offset \n", u64Position, u32CurPosition);
	}
	
	return fseek((FILE*)pFile, u64Position, SEEK_SET); 
} 

E_AACDEC_FLOW AACDecoder_FirstFrame_Callback(void *pUser_data,char *pcPcmbuf, unsigned int u32Pcmbuf_length,S_AACDEC_FRAME_INFO	*psFrame_info) 
{
	return eAACDEC_FLOW_CONTINUE;
} 


E_AACDEC_FLOW AACDecoder_Frame_Callback(void *pUser_data,char *pcPcmbuf,unsigned int u32Pcmbuf_length,S_AACDEC_FRAME_INFO	*psFrame_info)
{
	return eAACDEC_FLOW_CONTINUE;
} 

void AACDecoder_Terminal_Callback(void *pUser_data)
{

} 

void AACDecoder_Info_Callback(void *pUser_data,S_AACDEC_FRAME_INFO	*psFrame_info)
{

} 

/*! \fn CRED_Errors_t AUDIO_Close( AUDIO audio) 
    \brief AUDIO_Close : Close Dsp and mixer after play audio AAC.
    \param AUDIO audio a structure that contains all the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Close( AUDIO audio) 
{
	return CRED_NO_ERROR;
} 
/*! \fn CRED_Errors_t AUDIO_Close( AUDIO audio) 
    \brief AUDIO_Close : Close Dsp and mixer after play audio AAC.
    \param AUDIO audio a structure that contains all the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_fd_Close( AUDIO *audio) 
{
	return CRED_NO_ERROR;
} 
/*! \fn CRED_Errors_t AUDIO_Open(AUDIO *audio)
    \brief Open Dsp to play audio AAC.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Open(AUDIO *audio)
{
	return CRED_NO_ERROR;
}
/*! \fn CRED_Errors_t AUDIO_Set_Right_Volume(unsigned int volumeS)
    \brief Set volume of the output audio.
    \param volumeS: volume.
    \param mixer_fd: mixer file descriptor.
*/
CRED_Errors_t AUDIO_Set_Right_Volume(unsigned int volumeS)
{	
	CRED_Errors_t err = CRED_NO_ERROR;
	
	long min, max;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "Playback";
	
    
    snd_mixer_open(&handle, 0);
    snd_config_update_free_global();
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);
	if (elem) {

		snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
			
		rightChannelVolume= volumeS >100 ? max:volumeS;
		snd_mixer_selem_set_playback_volume(elem, (snd_mixer_selem_channel_id_t) 1, rightChannelVolume*max/100);
		snd_mixer_selem_set_playback_volume(elem, (snd_mixer_selem_channel_id_t) 0, leftChannelVolume*max/100);

     //~ snd_mixer_selem_set_playback_volume(elem, (snd_mixer_selem_channel_id_t) 0, 0);
	}
	else
	{
		AUDIO_TRACE_ERROR("Set playback volume failed\n");

	}

    snd_mixer_close(handle);

	return err;
}

/*! \fn CRED_Errors_t AUDIO_Set_Left_Volume(unsigned int volumeS)
    \brief Set volume of the output audio.
    \param volumeS: volume.
    \param mixer_fd: mixer file descriptor.
*/
CRED_Errors_t AUDIO_Set_Left_Volume(unsigned int volumeS)
{	
	CRED_Errors_t err = CRED_NO_ERROR;

	long min, max;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "Playback";
	
	
    snd_mixer_open(&handle, 0);
    snd_config_update_free_global();
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);
	if (elem) {
    
		snd_mixer_selem_get_playback_volume_range(elem, &min, &max);      
		leftChannelVolume= volumeS >100 ? max:volumeS;
		snd_mixer_selem_set_playback_volume(elem, (snd_mixer_selem_channel_id_t) 0, leftChannelVolume*max/100);
		snd_mixer_selem_set_playback_volume(elem, (snd_mixer_selem_channel_id_t) 1, rightChannelVolume*max/100);
	}
   	else
	{
		AUDIO_TRACE_ERROR("Set playback volume failed\n");

	}

    snd_mixer_close(handle);
	return err;
	
}
/*! \fn CRED_Errors_t AUDIO_Get_Volume(unsigned int volumeS,AUDIO *audio)
    \brief Get volume to play audio AAC.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
		   const int *volume value of volume ;
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Get_Volume(const int *volume,AUDIO *audio)
{

	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio to get volume\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	audio->mixer_out_fd = open("/dev/mixer", O_RDWR);
	if (audio->mixer_out_fd < 0) 
	{
		AUDIO_TRACE_ERROR("open(/dev/mixer) failed");
		return CRED_ERROR_DEVICE;	
	}
	
	if(ioctl(audio->mixer_out_fd, MIXER_READ(SOUND_MIXER_PCM), volume)==-1) 
	{
		AUDIO_TRACE_ERROR("Failed MIXER_READ volume \n");
		return CRED_ERROR_DEVICE;
	}
	close(audio->mixer_out_fd);
	
	return CRED_NO_ERROR ;		
}	

/*! \fn CRED_Errors_t AUDIO_Get_OGAIN(const int *gain,AUDIO *audio)
    \brief Get Output Gain for mixer.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
		   const int gain value of gain.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Get_OGAIN(int *gain,AUDIO *audio)
{
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio to get ouput gain\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio->mixer_out_fd = open("/dev/mixer", O_RDWR);
	if (audio->mixer_out_fd < 0) 
	{
		AUDIO_TRACE_ERROR("open(/dev/mixer) failed");
		return CRED_ERROR_DEVICE;	
	}
	
	if(ioctl(audio->mixer_out_fd, MIXER_READ(SOUND_MIXER_OGAIN), gain)==-1) 
	{
		AUDIO_TRACE_ERROR("Failed MIXER_READ volume \n");
		return CRED_ERROR_DEVICE;
	}
	
	close(audio->mixer_out_fd);
	
	return CRED_NO_ERROR ;		
}

/*! \fn CRED_Errors_t AUDIO_Set_OGAIN(const int *gain,AUDIO *audio)
    \brief Set Output Gain for mixer.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
		   unsigned int gain value of gain.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Set_OGAIN(unsigned int gain,AUDIO *audio)
{
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio to set ouput gain\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio->mixer_out_fd = open("/dev/mixer", O_RDWR);
	if (audio->mixer_out_fd < 0) 
	{
		AUDIO_TRACE_ERROR("open(/dev/mixer) failed");
		return CRED_ERROR_BAD_FD;	
	}
	const int audio_gain = (gain << 8) | gain;
	
	if(ioctl(audio->mixer_out_fd,MIXER_WRITE(SOUND_MIXER_OGAIN), &audio_gain)== -1)
	{
		AUDIO_TRACE_ERROR("Failed MIXER_WRITE to Set Gain \n");
		return CRED_ERROR_DEVICE;	
	}
	
	close(audio->mixer_out_fd);
	
	return CRED_NO_ERROR;
}
/*! \fn CRED_Errors_t AUDIO_Set_OGAIN(const int *gain,AUDIO *audio)
    \brief Set Output Gain for mixer.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
		   unsigned int gain value of gain.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Set_IGAIN(unsigned int gain,AUDIO *audio)
{	
	int mixer_fd;
	int audio_gain,gain_tmp;
	gain_tmp = gain;
	if(gain_tmp > 100)
	{
		gain_tmp = 100;
	}
	audio_gain = gain_tmp;
	mixer_fd = open("/dev/mixer", O_RDWR);
	
	if (mixer_fd  < 0) 
	{
		AUDIO_TRACE_ERROR("open(/dev/mixer1) failed");
		return CRED_ERROR_BAD_FD;	
	}
	/*audio_gain = (gain_tmp << 8) | gain_tmp;*/
	audio_gain <<= 8;
	audio_gain |= gain_tmp;
	ioctl(mixer_fd , MIXER_WRITE(SOUND_MIXER_PCM), &audio_gain);
	AUDIO_TRACE_INFO("MIC GAIN 0x%x\n",audio_gain);
	/*if(ioctl(mixer_fd,MIXER_WRITE(SOUND_MIXER_IGAIN), &audio_gain)== -1)
	{
		AUDIO_TRACE_ERROR("Failed MIXER_WRITE to Set Gain \n");
		return CRED_ERROR_DEVICE;	
	}
	if(ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_MIC), &audio_gain)==-1) 
	{
		AUDIO_TRACE_ERROR("Failed MIXER_READ volume \n");
		return CRED_ERROR_DEVICE;
	}
	if(ioctl(mixer_fd, MIXER_WRITE(SOUND_MIXER_PCM), &audio_gain)==-1) 
	{
		AUDIO_TRACE_ERROR("Failed MIXER_READ volume \n");
		return CRED_ERROR_DEVICE;
	}
    //recsrc = SOUND_MASK_MIC;
    recsrc = SOUND_MASK_PCM;
	if (ioctl(mixer_fd, SOUND_MIXER_WRITE_RECSRC,&recsrc) == -1) 
	{
    	printf("CD");
		return;
	}*/
	close(mixer_fd);
	
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_Resume(AUDIO *audio) 
    \brief Resume play AAC process.
    \param AUDIO *audio a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/						
CRED_Errors_t AUDIO_Resume(AUDIO *audio) 
{	
	struct timespec	sSleepTime = { 0, 0 };

	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio\n");
		return CRED_ERROR_BAD_PARAMETER;
	}

	pthread_mutex_lock(&audio->g_sPlayerStateMutex);

	if (audio->g_eAACPlayer_State != eAACPLAYER_STATE_PAUSED)
	{
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it must be paused\n");
		return CRED_ERROR_AUDIO_DECODE_RESUME; 
	} 
	
	audio->g_eAACPlayer_State = eAACPLAYER_STATE_TORESUME;
	pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
	
	while (1) 
	{
		nanosleep(&sSleepTime, NULL);

		if (audio->g_eAACPlayer_State == eAACPLAYER_STATE_PLAYING)
		{
			break;
		} 

	} 
	
return CRED_NO_ERROR;

}

/*! \fn CRED_Errors_t AUDIO_Pause(AUDIO *audio) 
    \brief Pause play AAC process.
    \param AUDIO *audio a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Pause(AUDIO *audio) 
{	
	struct timespec	sSleepTime = { 0, 0 };
	
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pthread_mutex_lock(&audio->g_sPlayerStateMutex);
	
	if (audio->g_eAACPlayer_State != eAACPLAYER_STATE_PLAYING)
	{
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it must be playing\n");
		return CRED_ERROR_AUDIO_DECODE_PAUSE; 
	} 
		
	audio->g_eAACPlayer_State = eAACPLAYER_STATE_TOPAUSE;
	pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
	
	while (1) 
	{
		nanosleep(&sSleepTime, NULL); 

		if (audio->g_eAACPlayer_State == eAACPLAYER_STATE_PAUSED) 
		{
			break;
		} 


	} 
	
	return CRED_NO_ERROR;
} 

/*! \fn CRED_Errors_t AUDIO_Stop(AUDIO *audio)
    \brief Stop play AAC process.
    \param AUDIO *audio a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Stop(AUDIO *audio) 
{
	CRED_Errors_t err = CRED_NO_ERROR;
	//struct timespec	sSleepTime = { 0, 0 };
	int output_audio_ch;
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&audio_play_mutex);
	output_audio_ch = audio->output_type;
	AUDIO_TRACE_INFO("Start audio flush \n");
	pthread_mutex_lock(&audio->g_sPlayerStateMutex);
	if (audio->g_eAACPlayer_State == eAACPLAYER_STATE_STOPPED)
	{
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it is stopped \n");
		if(output_audio_ch == PLAY_ON_LEFT)
		{
			OUTPUT_left_Term();
		}else if(output_audio_ch == PLAY_ON_RIGHT)
		{
			OUTPUT_right_Term();
		}
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_AUDIO_DECODE_STOP; 
	} 
	AUDIO_TRACE_INFO("Stop audio end 0\n");
	pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
	AUDIO_TRACE_INFO("Stop audio end 1\n");
	if (audio->s_bDecodeThread_Alive || audio->s_bPlaybackThread_Alive) 
	{
		pthread_mutex_lock(&audio->g_sPlayerStateMutex);
		audio->g_eAACPlayer_State = eAACPLAYER_STATE_TOSTOP;
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		do 
		{	
		  usleep(200);
		} while (audio->s_bDecodeThread_Alive || audio->s_bPlaybackThread_Alive);
	}
	AUDIO_TRACE_INFO("Stop audio end 2\n");
	if(output_audio_ch == PLAY_ON_LEFT)
	{
		OUTPUT_left_Term();
	}else if(output_audio_ch == PLAY_ON_RIGHT)
	{
		OUTPUT_right_Term();
	}
	pthread_mutex_unlock(&audio_play_mutex);
	AUDIO_TRACE_INFO("Stop audio end 3\n");
	return err;
} 


static void AUDIO_AACDecoder_Flash(AVCodecContext *dec_ctx, AVPacket *pkt, AVFrame *frame)
{
    int i, ch;
    int ret, data_size;
    FILE *outfile;
    
    /* open /dev/null */
	outfile=fopen("/dev/null","w");
	if(outfile)
	{
		goto On_Error;
	}
    /* send the packet with the compressed data to the decoder */
    ret = avcodec_send_packet(dec_ctx, pkt);
    if (ret < 0) {
        fprintf(stderr, "Error submitting the packet to the decoder\n");
        exit(1);
    }
    
    /* read all the output frames (in general there may be any number of them */
    while (ret >= 0) {
        ret = avcodec_receive_frame(dec_ctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return;
        else if (ret < 0) {
            AUDIO_TRACE_ERROR("Error during decoding\n");
            exit(1);
        }
        data_size = av_get_bytes_per_sample(dec_ctx->sample_fmt);
        if (data_size < 0) {
            /* This should not occur, checking just for paranoia */
            AUDIO_TRACE_ERROR("Failed to calculate data size\n");
            exit(1);
        }
        for (i = 0; i < frame->nb_samples; i++)
            for (ch = 0; ch < dec_ctx->channels; ch++)
                fwrite(frame->data[ch] + data_size*i, 1, data_size, outfile);
    }
On_Error:    
    fclose(outfile);
}
static int get_format_from_sample_fmt(snd_pcm_format_t *fmt,
                                      enum AVSampleFormat sample_fmt)
{
    int i;
    struct sample_fmt_entry {
        enum AVSampleFormat sample_fmt;  snd_pcm_format_t fmt_le;
    } sample_fmt_entries[] = {
        { AV_SAMPLE_FMT_U8,  SND_PCM_FORMAT_U8   },
        { AV_SAMPLE_FMT_S16, SND_PCM_FORMAT_S16_LE  },
        { AV_SAMPLE_FMT_S32, SND_PCM_FORMAT_S32_LE  },
        { AV_SAMPLE_FMT_FLT, SND_PCM_FORMAT_FLOAT_LE },
        { AV_SAMPLE_FMT_DBL, SND_PCM_FORMAT_FLOAT64_LE  },
    };
    for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
        struct sample_fmt_entry *entry = &sample_fmt_entries[i];
        if (sample_fmt == entry->sample_fmt) {
            *fmt = entry->fmt_le;
            return 0;
        }
    }
    AUDIO_TRACE_ERROR(
            "sample format %s is not supported as output format\n",
            av_get_sample_fmt_name(sample_fmt));
    return -1;
}

/*! \fn CRED_Errors_t AUDIO_AACDecoder_Init()
    \brief Function to initialize the AAC Decoder.
    \param none
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_AACDecoder_Init(AACDecoder_Params_Str *Decoder_Params)
{
	    
	    Decoder_Params->pkt = av_packet_alloc();
		
		/* find the AAC audio decoder */
		Decoder_Params->codec = avcodec_find_decoder(AV_CODEC_ID_AAC);
		if (!Decoder_Params->codec) {
			AUDIO_TRACE_ERROR("Codec not found\n");
			goto EndOfDecodeThread;
		}
		Decoder_Params->parser = av_parser_init(Decoder_Params->codec->id);
		if (!Decoder_Params->parser) {
			AUDIO_TRACE_ERROR("Parser not found\n");
			goto EndOfDecodeThread;
		}
		Decoder_Params->c = avcodec_alloc_context3(Decoder_Params->codec);
		if (!Decoder_Params->c) {
			AUDIO_TRACE_ERROR("Could not allocate audio codec context\n");
			goto EndOfDecodeThread;
		}
		/* open it */
		if (avcodec_open2(Decoder_Params->c, Decoder_Params->codec, NULL) < 0) {
			AUDIO_TRACE_ERROR("Could not open codec\n");
			goto EndOfDecodeThread;
		}
		// Set up SWR context once we've got codec information
		
		printf("Initial sample rate is %d\n",Decoder_Params->c->sample_rate);
		swr = swr_alloc();
		av_opt_set_int(swr, "in_channel_layout",  AV_CH_LAYOUT_MONO, 0);
		av_opt_set_int(swr, "out_channel_layout", AV_CH_LAYOUT_MONO,  0);
		av_opt_set_int(swr, "in_sample_rate",    22050, 0); //should be set to 16000 if the audio file was recorded.
		av_opt_set_int(swr, "out_sample_rate",    22050, 0); //should be set to 16000 if the audio file was recorded.
		av_opt_set_sample_fmt(swr, "in_sample_fmt",  AV_SAMPLE_FMT_FLTP, 0);
		av_opt_set_sample_fmt(swr, "out_sample_fmt", AV_SAMPLE_FMT_S16,  0);
		swr_init(swr);

	   return CRED_NO_ERROR;
EndOfDecodeThread:	
   return CRED_ERROR_INIT;
}

/*! \fn CRED_Errors_t AUDIO_AACDecoder_Decode()
    \brief Function to decode.
    \param none
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_AACDecoder_Decode(AACDecoder_Params_Str *Decoder_Params,char *out_DecodedData, int *out_DecodedSz)
{
	     int i, ch;
         int ret, ByteDecoded,i32DecodedSize=0;

           if (!Decoder_Params->decoded_frame) {
               if (!(Decoder_Params->decoded_frame = av_frame_alloc())) {
                   AUDIO_TRACE_ERROR("Could not allocate audio frame\n");
                   return CRED_ERROR_DEVICE;
               }
           }
   
           ret = av_parser_parse2(Decoder_Params->parser, Decoder_Params->c, &Decoder_Params->pkt->data, &Decoder_Params->pkt->size,
                                  Decoder_Params->data, Decoder_Params->data_size,
                                  AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
           if (ret < 0) {
               AUDIO_TRACE_ERROR("Error while parsing\n");
                   return CRED_ERROR_DEVICE;
           }
           Decoder_Params->data      += ret;
           Decoder_Params->data_size -= ret;
   
           if (Decoder_Params->pkt->size)
           {

     
         /* send the packet with the compressed data to the decoder */
         ret = avcodec_send_packet(Decoder_Params->c, Decoder_Params->pkt);
         if (ret < 0) {
           AUDIO_TRACE_ERROR("Error submitting the packet to the decoder\n");
                 return CRED_ERROR_DEVICE;
         }
     
         /* read all the output frames (in general there may be any number of them */
         while (ret >= 0) {
             ret = avcodec_receive_frame(Decoder_Params->c, Decoder_Params->decoded_frame);
             if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
             {
				 *out_DecodedSz=i32DecodedSize;
                 return CRED_NO_ERROR;
			 }
             else if (ret < 0) {
                 AUDIO_TRACE_ERROR("Error during decoding\n");
                   return CRED_ERROR_DEVICE;
             }
             ByteDecoded = av_get_bytes_per_sample(Decoder_Params->c->sample_fmt);
             if (ByteDecoded < 0) {
                 /* This should not occur, checking just for paranoia */
				 *out_DecodedSz=i32DecodedSize;

                 return CRED_NO_ERROR;
              }
             //~ for (i = 0; i < Decoder_Params->decoded_frame->nb_samples; i++)
                //~ for (ch = 0; ch < Decoder_Params->c->channels; ch++)
                    //~ memcpy(out_DecodedData+i32DecodedSize+ByteDecoded*i,Decoder_Params->decoded_frame->data[ch] + ByteDecoded*i,ByteDecoded);
				   
		    //~ i32DecodedSize+=ByteDecoded*Decoder_Params->decoded_frame->nb_samples*Decoder_Params->c->channels;
			swr_convert(swr,&out_DecodedData, Decoder_Params->decoded_frame->nb_samples, Decoder_Params->decoded_frame->extended_data, Decoder_Params->decoded_frame->nb_samples);   

		    i32DecodedSize+=2*Decoder_Params->decoded_frame->nb_samples*1;
			}

		   }
    *out_DecodedSz=i32DecodedSize;
	
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_AACDecoder_DeInit()
    \brief Function to initialize the AAC Decoder.
    \param none
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_AACDecoder_DeInit(AACDecoder_Params_Str *Decoder_Params,AUDIO *audio)
{    
	
	enum AVSampleFormat sfmt;
    int n_channels = 0;
    snd_pcm_format_t fmt;
    int ret;
		 
     /* flush the decoder */
    Decoder_Params->pkt->data = NULL;
    Decoder_Params->pkt->size = 0;
    AUDIO_AACDecoder_Flash(Decoder_Params->c, Decoder_Params->pkt, Decoder_Params->decoded_frame);
    
    /* print output pcm informations, because there have no metadata of pcm */
    sfmt = Decoder_Params->c->sample_fmt;
    if (av_sample_fmt_is_planar(sfmt)) {
        const char *packed = av_get_sample_fmt_name(sfmt);
        AUDIO_TRACE_INFO("Warning: the sample format the decoder produced is planar\n "
               "(%s). This example will output the first channel only.\n",
               packed ? packed : "?");
        sfmt = av_get_packed_sample_fmt(sfmt);
    }
    
    n_channels = Decoder_Params->c->channels;
    if ((ret = get_format_from_sample_fmt(&fmt, sfmt)) < 0)
        goto EndOfDecodeThread;
	
	AlsaPcmFormat=SND_PCM_FORMAT_S16_LE;
	audio->g_u32AACPlayer_ChannelNum = n_channels; 
	audio->g_u32AACPlayer_SampleRate = 11025; 
    printf("sample rate =%d\n",Decoder_Params->c->sample_rate);		
		
EndOfDecodeThread:	
   return CRED_NO_ERROR;
}

/*! \fn void s_DecodeThread(void *audio) 	
    \brief Thread to decode AAC frames.
    \param void *audio a structure that contains all the inputs/outputs AAC parameters.
    \retval void:any value to return.
*/
void s_DecodeThread(void *audio) 
{
	decode_status = 1;
	AUDIO *autio_th = (AUDIO *)audio;
	bool	bIsParseEnd ; 
	int value_ptr;
	bool	bContinueDecode;
	struct timespec	sSleepTime = { 0, 0 };
	S_AACDEC_FILE_CALLBACK	s_sAACDec_Callback;
	int	i32DecodedSize,i;
	struct stat	sFileStat; 
	CRED_Errors_t err = CRED_NO_ERROR;
	FILE *fd = NULL;
    uint8_t inbuf[AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
    int len,ch, ret, data_size;
    char pcDecodedPCM[AUDIO_INBUF_SIZE];
	AACDecoder_Params_Str AACDecoder_Params; 
	pthread_detach(pthread_self());
	if (audio == NULL)
	{
		AUDIO_TRACE_INFO("empty struct audio\n");
		goto EndOfDecodeThread;
	}

	for(i=0;i<autio_th->files_decode_nbr;i++)
	{
		autio_th->s_psAACFile = fopen(autio_th->files_decode_paths[i],"rb");
	
		if (autio_th->s_psAACFile == NULL)
		{
			AUDIO_TRACE_ERROR("erreur File Open %d:%s\n",i,autio_th->files_decode_paths[i]);
			goto EndOfDecodeThread;
		}
		
		autio_th->s_i32DecFrame_ReadIdx = 0;
		autio_th->s_i32DecFrame_WriteIdx = 0;
		autio_th->s_i32DecFrame_FullNum = 0; 
		
		memset(autio_th->s_apcDecFrame_Buf, 0, sizeof(autio_th->s_apcDecFrame_Buf));
		memset((void*)autio_th->s_ai32DecFrame_BufSize, 0, sizeof(autio_th->s_ai32DecFrame_BufSize));
		memset((void*)autio_th->s_ai32DecFrame_PCMSize, 0, sizeof(autio_th->s_ai32DecFrame_PCMSize));
		
		pthread_mutex_lock(&autio_th->g_sPlayerStateMutex);
		
		autio_th->s_bDecodeThread_Alive = true;
		
		if (fstat(fileno(autio_th->s_psAACFile), &sFileStat) < 0) 
		{
			AUDIO_TRACE_ERROR("fstat() failed\n"); 
			err= AUDIO_Close(*autio_th); 
			if (err != CRED_NO_ERROR)
			{
				AUDIO_TRACE_ERROR(" Close audio error %d\n",err);	
			}
			goto EndOfDecodeThread;
		} 
		//~ s_sAACDec_Callback.m_pfnRead = AACDecoder_Read_Callback;  
		//~ s_sAACDec_Callback.m_pfnSeek = AACDecoder_Seek_Callback;
		//~ s_sAACDec_Callback.m_pfnInfo_callback = AACDecoder_Info_Callback;
		//~ s_sAACDec_Callback.m_pfnFirst_frame_callback = AACDecoder_FirstFrame_Callback;
		//~ s_sAACDec_Callback.m_pfnFrame_callback = AACDecoder_Frame_Callback;
		//~ s_sAACDec_Callback.m_pfnTerminal_callback = AACDecoder_Terminal_Callback;
		//~ s_sAACDec_Callback.m_pFile = (void*)autio_th->s_psAACFile; 
		//~ s_sAACDec_Callback.m_u32File_size = sFileStat.st_size; 
		//~ autio_th->g_sAACDec_FrameInfo.m_i32Prescan_frames = eAACDEC_PRESCAN_DEFAULT;
//~ 
		//~ bIsParseEnd = false; 
		
		AUDIO_AACDecoder_Init(&AACDecoder_Params);
		
		//~ pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
	
		
		/* decode until eof */
		AACDecoder_Params.data      = inbuf;
		AACDecoder_Params.data_size = fread(inbuf, 1, AUDIO_INBUF_SIZE, autio_th->s_psAACFile);

		pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);

		if(autio_th->operation_type == AUDIO_PLAY_FILES)
		{
			autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_PLAYING;
		} 
		while (AACDecoder_Params.data_size > 0) 
		{	
			pthread_mutex_lock(&autio_th->g_sPlayerStateMutex);

			if(autio_th->operation_type == AUDIO_PLAY_FILES)
			{
				if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TOSTOP)
				{
					pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
					break;
				}
				
				if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TOPAUSE)
				{
					autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_PAUSED;
					
					if (autio_th->operation == live)
					{
						err = AUDIO_Get_Volume(&volume_tmp,autio_th);
						if (err != CRED_NO_ERROR)
						{
							//~ AUDIO_TRACE_ERROR(" Get_Volume failed with error %d\n",err);	
						//~ }
						//~ 
						//~ if (err != CRED_NO_ERROR)
						//~ {
							AUDIO_TRACE_ERROR(" Set_Volume failed with error %d\n",err);	
						}
					}
					else
					{
						pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
						nanosleep(&sSleepTime, NULL);					
						continue;
					}
				}
					
				if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_PAUSED)
				{
						if (autio_th->operation != live) 
						{
							pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
							continue;
						}
				}

				if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TORESUME) 
				{
					autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_PLAYING;
				}
			}
			pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
			
			if (autio_th->s_i32DecFrame_FullNum < DECFRAME_BUFNUM)
			{
				//~ bContinueDecode = AACDec_DecodeFrame();
				
				    AUDIO_AACDecoder_Decode(&AACDecoder_Params,pcDecodedPCM,&i32DecodedSize);
				    if(i32DecodedSize)
				    {
					if ((autio_th->s_ai32DecFrame_BufSize[autio_th->s_i32DecFrame_WriteIdx] < i32DecodedSize)/* && (autio_th->operation_type != AUDIO_DECODE_FILES)*/)
					{

						autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx] = realloc((void*)(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx]), i32DecodedSize);

						if (autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx] == NULL) 
						{
							AUDIO_TRACE_ERROR("realloc() AAC Frame for decode failed \n"); 
							break;
						} 

						autio_th->s_ai32DecFrame_BufSize[autio_th->s_i32DecFrame_WriteIdx] = i32DecodedSize;
						//memcpy((void*)(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx]), autio_th->g_sAACDec_FrameInfo.m_pcSample_buffer, i32DecodedSize);
				        
				        //~ pcDecodedPCM = (char*)(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx]); 

				    }
					if(autio_th->operation_type == AUDIO_DECODE_FILES)
					{
						if((i == 0) && (autio_th->s_i32DecFrame_WriteIdx == 0))
						{
							fd = fopen(autio_th->on_board_msg, "w");
							if(fd == NULL)
							{
								AUDIO_TRACE_ERROR("open pcm file error!\n");
								goto EndOfDecodeThread;
							}
						}
						//~ for (i = 0; i < decoded_frame->nb_samples; i++)
						//~ for (ch = 0; ch < c->channels; ch++)
							//~ fwrite(decoded_frame->data[ch] + data_size*i, 1, data_size, fd);
						//fwrite(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx], 1, i32DecodedSize, fd);
						//~ fseek(fd,0,SEEK_END);
						fwrite(pcDecodedPCM, 1, i32DecodedSize, fd);
					}
					
					if(autio_th->operation_type != AUDIO_DECODE_FILES)
					{					
						//~ for (i = 0; i < decoded_frame->nb_samples; i++)
						//~ {
						//~ for (ch = 0; ch < c->channels; ch++)
						//~ {
							//~ memcpy(pcDecodedPCM+data_size*i, decoded_frame->data[ch] + data_size*i, data_size);
                        //~ }
					    //~ }
					    memcpy(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_WriteIdx],pcDecodedPCM , i32DecodedSize);
                        
					}
				
					autio_th->s_ai32DecFrame_PCMSize[autio_th->s_i32DecFrame_WriteIdx] = i32DecodedSize;  
					autio_th->s_i32DecFrame_FullNum++;
					autio_th->s_i32DecFrame_WriteIdx++;

					if ( autio_th->s_i32DecFrame_WriteIdx == DECFRAME_BUFNUM ) 
						autio_th->s_i32DecFrame_WriteIdx = 0;
			     }
				if (AACDecoder_Params.data_size < AUDIO_REFILL_THRESH) {
					memmove(inbuf, AACDecoder_Params.data, AACDecoder_Params.data_size);
					AACDecoder_Params.data = inbuf;
					len = fread(AACDecoder_Params.data + AACDecoder_Params.data_size, 1,
								AUDIO_INBUF_SIZE - AACDecoder_Params.data_size, autio_th->s_psAACFile);
					if (len > 0)
						AACDecoder_Params.data_size += len;
				}
			}
			
					
				//bContinueDecode = AACDec_DecodeFrame();
				//~ if (autio_th->g_sAACDec_FrameInfo.m_eError == eAACDEC_ERROR_NONE)
				//~ {
					//~ if (autio_th->g_sAACDec_FrameInfo.m_u32Samples <= 0)
						//~ continue; 
					//~ 
					//~ i32DecodedSize = 2 * autio_th->g_sAACDec_FrameInfo.m_u32Samples * autio_th->g_sAACDec_FrameInfo.m_u32Channels;
				//~ } 
				//~ else 
				//~ {
					//~ AUDIO_TRACE_ERROR("Decode AAC failed \n"); 
					//~ MsEvent.As_event = CRED_AS_MULTIMEDIA;
					//~ Multimedia_event.multimedia_action = CRED_MS_AAC_DECODE_FRAME;
					//~ MsEvent.event_detail = (uint8_t*)&(Multimedia_event.multimedia_action);
					//~ Multimedia_event.event_detail = NULL;
					//~ //MS_Notify(MsEvent);
					//~ if(audio_ms_notify)
						//~ (*audio_ms_notify)(MsEvent);
					//~ break;
				//~ }
			
			else 
			{
				nanosleep(&sSleepTime, NULL);
			}
		}
		
		if(autio_th->operation_type == AUDIO_DECODE_FILES)
		{
			if (autio_th->s_psAACFile)
			{
				audio_DeleteFSystemCache(autio_th->s_psAACFile);
				fclose(autio_th->s_psAACFile);
				autio_th->s_psAACFile = NULL;
			}
		}
		
		AUDIO_AACDecoder_DeInit(&AACDecoder_Params,autio_th);
		//AACDec_Finalize();		
	}

EndOfDecodeThread:

	if((autio_th->operation_type == AUDIO_DECODE_FILES) && (fd != NULL))
	{
		audio_DeleteFSystemCache(fd);
		fclose(fd);
	}

	autio_th->s_bDecodeThread_Alive = false;
	autio_th->operation_type = AUDIO_TYPE_NOT_USED;
	AUDIO_TRACE_INFO("Exit decode thread \n");
	decode_status = 0;
	pthread_exit(&value_ptr);
}
/*! \fn void s_PlaybackThread(void *audio) 	
    \brief Thread to play AAC frames.
    \param void *audio a structure that contains all the inputs/outputs AAC parameters.
    \retval void:any value to return.
*/
void s_PlaybackThread(void *audio) 
{
	
	AUDIO *autio_th = (AUDIO *)audio;
	char	*pcDecodedPCM ;
	audio_buf_info	sDevDSP_Info; 
	int	i32FillBytes, ui32SampleRate;
	struct timespec	sSleepTime = { 0, 0 };
	int value_ptr;
	int free_size ;
	fd_set	sWriteFDSet; 
	volatile unsigned int g_u32AACPlayer_PlaySampleCnt = 0; 
	CRED_Errors_t err = CRED_NO_ERROR;
	int	i32Index,audiofd;
	pthread_detach(pthread_self());
	PlaybackThreadStatus =1;
	GsmSelectEnableSpeaker();
	if (audio == NULL)
	{
		AUDIO_TRACE_INFO("empty struct audio\n");
		goto EndOfPlayback;
	}
	autio_th->s_bPlaybackThread_Alive = true;
	
	if(OUTPUT_GetBlockSize(&autio_th->s_i32DevDSP_BlockSize) != CRED_NO_ERROR)
	{
	  AUDIO_TRACE_ERROR("SNDCTL_DSP_GETBLKSIZE ioctl error for palyback \n");
	}
		if(autio_th->output_type == PLAY_ON_LEFT)
		{	
			OUTPUT_PlaybackHardware_Left_Prepare();
			AUDIO_Set_Left_Volume(20);
		}
		else {
			OUTPUT_PlaybackHardware_Right_Prepare();
			AUDIO_Set_Right_Volume(20);
			
		}
		
	//~ ui32SampleRate=11025;//should be set to 16000 if the audio file was recorded.
    OUTPUT_SetSampleRate(&ui32SampleRate);//initial sample rate was changed while recording
	    /* */
	    
	if (autio_th->s_i32DevDSP_BlockSize> 0) 
	{
		g_u32AACPlayer_PlaySampleCnt = 0;

		while(1) 
		{

			pthread_mutex_lock(&autio_th->g_sPlayerStateMutex);

			if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TOSTOP) 
			{
				if(autio_th->output_type == PLAY_ON_LEFT)
				{	
					OUTPUT_PlaybackHardware_Left_Prepare();
				}
				else {
					OUTPUT_PlaybackHardware_Right_Prepare();
					
				}
				pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
				AUDIO_TRACE_INFO("eAACPLAYER_STATE_TOSTOP\n");
				break;
			}
			
			if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TOPAUSE)
			{
				autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_PAUSED;

				if(autio_th->output_type == PLAY_ON_LEFT)
				{	
					OUTPUT_PlaybackHardware_Left_Prepare();
				}
				else {
					OUTPUT_PlaybackHardware_Right_Prepare();
					
				}
				
				//~ if(autio_th->output_type == PLAY_ON_LEFT)
				//~ {	
				 //~ OUTPUT_PlaybackHardware_Left_Pause(1);
				//~ }
				//~ else {
				 //~ OUTPUT_PlaybackHardware_Right_Pause(1);
					
				//~ }
				if(autio_th->operation != live)
				{
					//~ OUTPUT_GetDspFd(&audiofd);
					//~ ioctl(audiofd, SNDCTL_DSP_SILENCE, 0);
					pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
					continue;
				}
				else 
				{
					//~ err = AUDIO_Get_Volume(&volume_tmp,autio_th);
					//~ if (err != CRED_NO_ERROR)
					//~ {
						//~ AUDIO_TRACE_ERROR(" Get_Volume failed with error %d\n",err);	
					//~ }
					
					//~ if(autio_th->output_type == PLAY_ON_LEFT)
					//~ {
						//~ err =AUDIO_Set_Left_Volume(0);
					//~ }else if(autio_th->output_type == PLAY_ON_RIGHT)
					//~ {
						//~ err = AUDIO_Set_Right_Volume(0);
					//~ }
					//~ if (err != CRED_NO_ERROR)
					//~ {
						//~ AUDIO_TRACE_ERROR(" Set_Volume failed with error %d\n",err);	
					//~ } 
				}
			}
		
			if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_PAUSED)
			{
				if (autio_th->operation != live) 
				{
					pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
					usleep(500);
					continue;
				}
			}
			if (autio_th->g_eAACPlayer_State == eAACPLAYER_STATE_TORESUME)
			{ 
				//~ if(autio_th->output_type == PLAY_ON_LEFT)
				//~ {	
				 //~ OUTPUT_PlaybackHardware_Left_Pause(0);
				//~ }
				//~ else {
				 //~ OUTPUT_PlaybackHardware_Right_Pause(0);
					
				//~ }
				
				if (autio_th->operation == live)
				{
					if(autio_th->output_type == PLAY_ON_LEFT)
					{
						err =AUDIO_Set_Left_Volume(volume_tmp);
					}else if(autio_th->output_type == PLAY_ON_RIGHT)
					{
						err = AUDIO_Set_Right_Volume(volume_tmp);
					}
					if (err != CRED_NO_ERROR)
					{
						AUDIO_TRACE_ERROR(" Set_Volume failed with error %d\n",err);	
					} 
				}
				 
				autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_PLAYING;
			}
			
			pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
			
			if (autio_th->s_i32DecFrame_FullNum > 0)
			{  
				pcDecodedPCM = (char*)(autio_th->s_apcDecFrame_Buf[autio_th->s_i32DecFrame_ReadIdx]); 

				while (autio_th->s_ai32DecFrame_PCMSize[autio_th->s_i32DecFrame_ReadIdx] > 0 )
				{

					if(autio_th->output_type == PLAY_ON_RIGHT)
					{
						free_size = OUTPUT_RGetFreeBufferSize();
						if (free_size == 0) 
						{
							usleep(10000);
							continue;
						} 
					}else if(autio_th->output_type == PLAY_ON_LEFT)
					{
						free_size = OUTPUT_LGetFreeBufferSize();
						if (free_size == 0) 
						{
							usleep(10000);
							continue;
						} 
					}else
					{
						AUDIO_TRACE_ERROR("unvalid play choice \n");
					}

					i32FillBytes = autio_th->s_ai32DecFrame_PCMSize[autio_th->s_i32DecFrame_ReadIdx]; 

					if (i32FillBytes > free_size)
						i32FillBytes = free_size;
     
					if (i32FillBytes > autio_th->s_i32DevDSP_BlockSize) 
						i32FillBytes = autio_th->s_i32DevDSP_BlockSize;

					if(autio_th->output_type == PLAY_ON_RIGHT)
					{
						err = OUTPUT_RWrite(pcDecodedPCM, i32FillBytes);
					}else if(autio_th->output_type == PLAY_ON_LEFT)
					{
						err = OUTPUT_LWrite(pcDecodedPCM, i32FillBytes);
						AUDIO_TRACE_INFO("write buffer \n");
					}else
					{
						AUDIO_TRACE_ERROR("unvalid play choice \n");
					}

					if (err != CRED_NO_ERROR)
					{
							AUDIO_TRACE_ERROR("write(/dev/dsp) failed \n");
							MsEvent.As_event = CRED_AS_MULTIMEDIA;
							Multimedia_event.multimedia_action = CRED_MS_AAC_WRITE_DSP;
							MsEvent.event_detail = (uint8_t*)&(Multimedia_event.multimedia_action);
							Multimedia_event.event_detail = NULL;
							//MS_Notify(MsEvent);
							if(audio_ms_notify)
								(*audio_ms_notify)(MsEvent);
							goto EndOfPlayback;		
					} 
					
					usleep(1000);
					g_u32AACPlayer_PlaySampleCnt += i32FillBytes / 2 / autio_th->g_u32AACPlayer_ChannelNum; 
					pcDecodedPCM += i32FillBytes;
					autio_th->s_ai32DecFrame_PCMSize[autio_th->s_i32DecFrame_ReadIdx] -= i32FillBytes;
					
				} 

				autio_th->s_i32DecFrame_FullNum--;
				autio_th->s_i32DecFrame_ReadIdx++;
				
				if ( autio_th->s_i32DecFrame_ReadIdx == DECFRAME_BUFNUM )
					autio_th->s_i32DecFrame_ReadIdx = 0;
			}
			else if (autio_th->s_bDecodeThread_Alive) 
			{				
				struct timespec	sSleepTime = { 0, 0 };
				nanosleep(&sSleepTime, NULL);
			} 
			else 
			{
				AUDIO_TRACE_INFO("End of file to play \n");
				
				MsEvent.As_event = CRED_AS_MULTIMEDIA;
				Multimedia_event.multimedia_action = CRED_MS_END_OF_FILE_PLAY_AAC;
				MsEvent.event_detail = (uint8_t*)&(Multimedia_event.multimedia_action);
				Multimedia_event.event_detail = NULL;
				//MS_Notify(MsEvent);
				if(audio_ms_notify)
					(*audio_ms_notify)(MsEvent);
				break;
			} 
		} 

	}

EndOfPlayback:
	while (autio_th->s_bDecodeThread_Alive)
	{
		AUDIO_TRACE_INFO("Exit Decode thread\n");
		usleep(100000);
	}
	autio_th->s_bDecodeThread_Alive = false;
	memset((void*)autio_th->s_ai32DecFrame_BufSize, 0, sizeof(autio_th->s_ai32DecFrame_BufSize));
	memset((void*)autio_th->s_ai32DecFrame_PCMSize, 0, sizeof(autio_th->s_ai32DecFrame_PCMSize));
	for (i32Index = DECFRAME_BUFNUM - 1; i32Index >= 0; i32Index--)
	{
		
		if (autio_th->s_apcDecFrame_Buf[i32Index]) 
		{
			free((void*)(autio_th->s_apcDecFrame_Buf[i32Index]));
		} 
	}
	
	if (autio_th->s_psAACFile)
	{
		fclose(autio_th->s_psAACFile);
		autio_th->s_psAACFile = NULL;
		
	}

	if(autio_th->output_type == PLAY_ON_LEFT)
	{
		while (OUTPUT_LGetFreeBufferSize() < OUTPUT_MAX_SIZE_TO_WRITE)
		{
			usleep(10000);
		}
	}else if(autio_th->output_type == PLAY_ON_RIGHT)
	{
		while (OUTPUT_RGetFreeBufferSize() < OUTPUT_MAX_SIZE_TO_WRITE)
		{
			usleep(10000);
		}
	}else
	{
		AUDIO_TRACE_ERROR("audio output type error\n");
	}
	
	pthread_mutex_lock(&autio_th->g_sPlayerStateMutex);
	autio_th->g_eAACPlayer_State = eAACPLAYER_STATE_STOPPED; 
	pthread_mutex_unlock(&autio_th->g_sPlayerStateMutex);
	
	autio_th->s_bPlaybackThread_Alive = false;
	AUDIO_TRACE_INFO("Exit Playback thread\n");
	PlaybackThreadStatus =0;
	pthread_exit(&value_ptr);
		
} 

/*! \fn CRED_Errors_t AUDIO_Play(char* pczFileName, AUDIO *audio) 
    \brief launch the decode and play thread.
    \param AUDIO *audio a structure that contains all the inputs/outputs AAC parameters.
    \param char* pczFileName file name to play.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
static int audio_run=0;
CRED_Errors_t AUDIO_Play(char* pczFileName, AUDIO *audio) 
{
	CRED_Errors_t err = CRED_NO_ERROR;
	struct stat sbuff;
	pthread_t 	sDecodeThread;
	pthread_t	sPlaybackThread;
	pthread_mutex_lock(&audio_play_mutex);
	if(audio_run == 1)
	{
		AUDIO_TRACE_ERROR( "Audio Device Used");
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio_run = 1;
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio\n");
		audio_run = 0;
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&audio->g_sPlayerStateMutex);
	if (audio->s_bDecodeThread_Alive)
	{
		AUDIO_TRACE_ERROR( "Audio Device Used");
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		audio_run = 0;
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}else
	{
		
		AUDIO_TRACE_INFO("\n Audio device not used %d\n",audio->s_bDecodeThread_Alive);
		audio->s_bDecodeThread_Alive = true;
	}
	
	if (!pczFileName ||(audio->g_eAACPlayer_State != eAACPLAYER_STATE_STOPPED)) 
	{
		AUDIO_TRACE_ERROR("erreur FileName or play state \n");
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		audio->s_bDecodeThread_Alive = false;
		audio_run = 0;
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	} 
	if(stat(pczFileName, &sbuff))
	{
		AUDIO_TRACE_ERROR("erreur FileName \n");
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		audio->s_bDecodeThread_Alive = false;
		audio_run = 0;
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio->files_decode_nbr = 1;
	memset(audio->files_decode_paths,0x00,sizeof(audio->files_decode_paths));
	strncpy(audio->files_decode_paths[0],pczFileName,strlen(pczFileName));
	audio->files_decode_nbr = 1;
	audio->operation_type = AUDIO_PLAY_FILES;
	audio->s_bDecodeThread_Alive = true;

	pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
	AUDIO_TRACE_ERROR("Start decode play\n");

	if (CRED_TOOLS_CreateTask("sDecodeThread", &sDecodeThread, 0x2000,s_DecodeThread, 99, SCHED_FIFO,(void *)audio)  < 0) 
	{ 
		audio->s_bDecodeThread_Alive = false;
		AUDIO_TRACE_ERROR(" erreur create decode thread");
		audio_run = 0;
		AUDIO_fd_Close(audio);
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	} 
	
	do
	{ 
		struct timespec	sSleepTime = { 0, 0 };
		nanosleep(&sSleepTime, NULL);
		
	} while (audio->s_bDecodeThread_Alive && audio->s_i32DecFrame_FullNum < DECFRAME_BUFNUM);

	if (audio->s_i32DecFrame_FullNum > 0) 
	{
		audio->s_bPlaybackThread_Alive = false;

		while (CRED_TOOLS_CreateTask("sPlaybackThread", &sPlaybackThread, 0x2000,s_PlaybackThread, 99, SCHED_FIFO,(void *)audio) < 0)
		{   
			AUDIO_TRACE_ERROR(" erreur create playback thread");
			audio_run = 0;
			AUDIO_fd_Close(audio);
			pthread_mutex_unlock(&audio_play_mutex);
			return CRED_ERROR_BAD_PARAMETER;
		}
	}
	else 
	{
		AUDIO_TRACE_ERROR(" Bad Dec Frame_FullNum ");
		audio_run = 0;
		AUDIO_fd_Close(audio);
		pthread_mutex_unlock(&audio_play_mutex);
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio_run = 0;
	pthread_mutex_unlock(&audio_play_mutex);
	return err;
} 

void s_EncodeThread(void* encode);
void s_RecordThread(void* encode);

/*! \fn CRED_Errors_t Close_Audio_Record(AUDIO_Enc encode) 
    \brief Close Dsp1 and mixer1 after record audio AAC.
    \param AUDIO_Enc: a structure that contains all the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t Close_Audio_Record(AUDIO_Enc encode) 
{
	if (encode.s_i32DevDSP >= 0) 
	{
		//~ close(encode.s_i32DevDSP);
		encode.s_i32DevDSP = -1;
	} 
	else
	{
		AUDIO_TRACE_ERROR( "Wrong dsp encode fd\n");
		return CRED_ERROR_BAD_FD;
	}
	
	if (encode.mixer_fd >=0)
	{
		close(encode.mixer_fd);
		encode.mixer_fd = -1;
	}
	else
	{
		AUDIO_TRACE_ERROR( "Wrong Mixer encode fd\n");
		return CRED_ERROR_BAD_FD;
	}
	return CRED_NO_ERROR;
} 

/*! \fn CRED_Errors_t s_OpenAudioRecDevice(int i32SampleRate,int i32ChannelNum,AUDIO_Enc *encode)
    \brief Open Dsp1 to record audio AAC.
    \param AUDIO_Enc *encode a structure that contains all the inputs/outputs AAC parameters.
    \param int i32SampleRate: SAmple rate for record.
    \param int i32ChannelNum: Numbers of channels,
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
#if 0
CRED_Errors_t s_OpenAudioRecDevice(int i32SampleRate,int i32ChannelNum,AUDIO_Enc *encode)
 {
	struct stat statbuf;
	int len;
	int oss_format;
	int audio_gain,gain_tmp;
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	encode->s_i32DevDSP_BlockSize_enc = 0;
	if(encode->input==micro) 
	{
		//~ encode->s_i32DevDSP = open("/dev/dsp1", O_RDWR);
		OUTPUT_GetDspFd(&encode->s_i32DevDSP);
		if (encode->s_i32DevDSP < 0) 
		{
			AUDIO_TRACE_ERROR( "Failed to open /dev/dsp1 \n");
			return CRED_ERROR_BAD_FD;
		} 
		oss_format=AFMT_S16_LE;
		//~ if(ioctl(encode->s_i32DevDSP, SNDCTL_DSP_SETFMT,&oss_format)== -1)
		//~ {
			//~ AUDIO_TRACE_ERROR("SNDCTL_DSP_RESET ioctl error for record aac \n");
		//~ }
		//~ fcntl(encode->s_i32DevDSP, F_SETFL, O_NONBLOCK);
//~ 
		if (ioctl(encode->s_i32DevDSP, SNDCTL_DSP_SPEED, &i32SampleRate) < 0) 
		{
			//~ Close_Audio_Record(*encode); 
			return CRED_ERROR_BAD_PARAMETER;
		} 

		//~ if(ioctl(encode->s_i32DevDSP, SNDCTL_DSP_CHANNELS, &i32ChannelNum) == -1)
		//~ {
			//~ AUDIO_TRACE_ERROR("SNDCTL_DSP_CHANNELS ioctl error for record aac \n");
		//~ }
		if(ioctl(encode->s_i32DevDSP, SNDCTL_DSP_GETBLKSIZE, &encode->s_i32DevDSP_BlockSize_enc) == -1)
		{
			AUDIO_TRACE_ERROR("SNDCTL_DSP_GETBLKSIZE ioctl error for record aac \n");
		}
		encode->mixer_fd = open("/dev/mixer", O_WRONLY);
		if (encode->mixer_fd < 0)  
		{
			AUDIO_TRACE_ERROR("open /dev/mixer1 error \n");
			return CRED_ERROR_BAD_FD;
		}
		gain_tmp = encode->mic_gain;
		if((gain_tmp > 100) || (gain_tmp==0))
		{
			gain_tmp = 100;
		}
		audio_gain = gain_tmp;
		audio_gain <<= 8;
		audio_gain |= gain_tmp;
		if(ioctl(encode->mixer_fd , MIXER_WRITE(SOUND_MIXER_PCM), &audio_gain) == -1)
		{
			AUDIO_TRACE_ERROR("MIC gain ioctl error for record aac \n");
			return CRED_ERROR_BAD_FD;
		}
		AUDIO_TRACE_ERROR("MIC GAIN 0x%x\n",audio_gain);
	}
	else	
	{
		if (stat("./test.pcm", &statbuf) == -1) 
		{
			AUDIO_TRACE_ERROR("erreur file pcm \n");
		}
		len = statbuf.st_size;
		
		encode->s_i32DevDSP = open("./test.pcm", O_RDONLY);
		if (encode->s_i32DevDSP < 0) 
		{
			AUDIO_TRACE_ERROR("Can not open pcm file %s\n");
			return CRED_ERROR_BAD_PARAMETER;
		}
		encode->s_i32DevDSP_BlockSize_enc = 0x800;		
		encode->s_i32no = (len/encode->s_i32DevDSP_BlockSize_enc)+1; 
		int pos = lseek(encode->s_i32DevDSP, 0x2C, SEEK_SET);
		if ( pos == -1 )
		{
			AUDIO_TRACE_ERROR("seek position error\n");
			return CRED_ERROR_BAD_PARAMETER;
		}
	}
	return CRED_NO_ERROR;
} 
#else

CRED_Errors_t s_OpenAudioRecDevice(int i32SampleRate,int i32ChannelNum,int *i32DevDSP_BlockSize)
 {

	
	int err;
    snd_pcm_hw_params_t *hw_params;
    int size;
    unsigned int val;

    if ((err = snd_pcm_open (&captureHandle, PCM_DEVICE, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    fprintf (stderr, "cannot open audio device %s (%s)", PCM_DEVICE, snd_strerror (err));
            goto on_error;
    }
    snd_config_update_free_global();

    if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
    {
    fprintf (stderr, "cannot allocate hardware parameter structure (%s)", snd_strerror (err));
            goto on_error;
    }

    if ((err = snd_pcm_hw_params_any (captureHandle, hw_params)) < 0)
    {
    fprintf (stderr, "cannot initialize hardware parameter structure (%s)", snd_strerror (err));
            goto on_error;
    }

    if ((err = snd_pcm_hw_params_set_access (captureHandle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
    fprintf (stderr, "cannot set access type (%s)",  snd_strerror (err));
            goto on_error;
    }

    if ((err = snd_pcm_hw_params_set_format (captureHandle, hw_params, SND_PCM_FORMAT_S16_LE  )) < 0)
    {
    fprintf (stderr, "cannot set sample format (%s)", snd_strerror (err));
            goto on_error;
    }

    unsigned int rate = i32SampleRate;
    if ((err = snd_pcm_hw_params_set_rate_near (captureHandle, hw_params, &rate, 0)) < 0)
    {
    fprintf (stderr, "cannot set sample rate (%s)", snd_strerror (err));
            goto on_error;
    }
  // Set the buffer time
    static unsigned int bufferTime = CAPTURE_BUFF_DURATION_US;
    int dir = 0;
    
    //~ err = snd_pcm_hw_params_set_buffer_time(capture_handle, hw_params, bufferTime, dir);
    //~ if (err < 0)
    //~ {
    //~ fprintf (stderr, "Unable to set buffer time %i for playback: %s\n", bufferTime, snd_strerror(err));
    //~ return err;
    //~ }
     //~ 
     
    snd_pcm_uframes_t wantedPeriodSize = CAPTURE_ALSA_PERIOD_SIZE;
    if ((err = snd_pcm_hw_params_set_period_size_near(captureHandle, hw_params, &wantedPeriodSize, NULL)) < 0)
    {
        fprintf (stderr, "Unable to set period size %i for record: %s\n", wantedPeriodSize, snd_strerror(err));
    }

 
    if ((err = snd_pcm_hw_params_set_channels (captureHandle, hw_params, 1)) < 0)
    {
    fprintf (stderr, "cannot set channel count (%s)", snd_strerror (err));
            goto on_error;
    }
    if ((err = snd_pcm_hw_params (captureHandle, hw_params)) < 0)
    {
    fprintf (stderr, "cannot set parameters (%s)", snd_strerror (err));
            goto on_error;
    }
	  /* We want to loop for 5 seconds */
   // snd_pcm_hw_params_get_period_time(hw_params,
	//									 &capture_period_time, &dir);

   /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(hw_params, &audio_recording_frames, &dir);
   *i32DevDSP_BlockSize= audio_recording_frames * 2; /* 2 bytes/sample, 2 channels */

    snd_pcm_hw_params_free (hw_params);

   return CRED_NO_ERROR;

on_error:
    snd_pcm_hw_params_free(hw_params);
    snd_pcm_close(captureHandle);
    return CRED_ERROR_INIT;
		
} 
#endif
/*! \fn CRED_Errors_t AUDIO_Record_Release(AUDIO_Enc *encode)
    \brief Release all allocated buffers used to record audio AAC.
    \param AUDIO_Enc *encode a structure that contains all the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/

CRED_Errors_t AUDIO_Record_Release(AUDIO_Enc *encode) 
{
	int	i32Index;
	
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	memset((void*)encode->s_ai32PCMFrame_PCMSize, 0, sizeof(encode->s_ai32PCMFrame_PCMSize));
	for (i32Index = PCMFRAME_BUFNUM - 1; i32Index >= 0; i32Index--)
	{ 
		if (encode->s_apcPCMFrame_Buf[i32Index]) 
		{
			free((void*)(encode->s_apcPCMFrame_Buf[i32Index]));
		}
	} 
	//~ if (encode->s_psAACFile_enc)
	//~ {
		//~ fclose(encode->s_psAACFile_enc);
		//~ encode->s_psAACFile_enc = NULL;
		//~ //sync();
	//~ } 
	
	// Stop capture flow and close device
	if(captureHandle!=NULL)
	{
    snd_pcm_drop(captureHandle);
    snd_pcm_reset(captureHandle);
    snd_pcm_prepare(captureHandle);
    snd_pcm_unlink(captureHandle);
    snd_pcm_hw_free(captureHandle);
    snd_pcm_close(captureHandle);
    captureHandle=NULL;
	}
	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_Record(char* pczFileName,AUDIO_Enc *encode)
    \brief launch the record and encode threads.
    \param AUDIO_Enc *encode a structure that contains all the inputs/outputs AAC parameters.
    \param char* pczFileName :record file.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_Record(char* pczFileName,AUDIO_Enc *encode)
{	
	pthread_t	sRecordThread;
	pthread_t 	sEncodeThread;
	int	i32Index;
	CRED_Errors_t err = CRED_NO_ERROR;
	
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex); 
	if (!pczFileName|| encode->g_eAACRecorder_State != eAACRECORDER_STATE_STOPPED)
	{
		AUDIO_TRACE_ERROR("error state or file name for record file\n");
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		return CRED_ERROR_BAD_PARAMETER;
	} 
	
	
	encode->s_psAACFile_enc = fopen(pczFileName, "wb"); 
	if (encode->s_psAACFile_enc == NULL) 
	{
		AUDIO_TRACE_ERROR(" error FileName \n");
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		return CRED_ERROR_BAD_PARAMETER;	
	} 
	encode->s_i32PCMFrame_FullNum = 0;
	memset((void*)encode->s_ai32PCMFrame_PCMSize, 0, sizeof(encode->s_ai32PCMFrame_PCMSize));
	for (i32Index = 0; i32Index < PCMFRAME_BUFNUM; i32Index++) 
	{
		encode->s_apcPCMFrame_Buf[i32Index] = malloc(PCMFRAME_BUFSIZE);
		encode->s_ai32PCMFrame_PCMSize[i32Index] = 0;
		if (encode->s_apcPCMFrame_Buf[i32Index] == NULL)
		{
			AUDIO_TRACE_ERROR("error malloc() buffer for encode \n");
			pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
			return CRED_ERROR_BAD_PARAMETER;
			
		} 
	}
	
	encode->s_bRecordThread_Alive = true;

	if (CRED_TOOLS_CreateTask("sRecordThread", &sRecordThread, 0x2000,s_RecordThread, 99, SCHED_FIFO,(void *)encode) != 0) 
	{
		encode->s_bRecordThread_Alive = false;
		AUDIO_TRACE_ERROR("erreur Record thread \n");
		err = AUDIO_Record_Release(encode); 
		
		if (err != CRED_NO_ERROR) 
		{
			AUDIO_TRACE_ERROR("failed to release allocated buffers for record %d\n",err);	
		}
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		return CRED_ERROR_BAD_PARAMETER;
		
	} 
	encode->s_bEncodeThread_Alive = false;

	if (CRED_TOOLS_CreateTask("sEncodeThread", &sEncodeThread, 0x2000,s_EncodeThread, 99, SCHED_FIFO,(void *)encode) != 0) 
	{
		AUDIO_TRACE_ERROR("erreur Encode thread \n");
		err = AUDIO_Record_Release(encode);
		
		if (err != CRED_NO_ERROR) 
		{
			AUDIO_TRACE_ERROR("failed to release allocated buffers for record %d\n",err);	

		}
		
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		return CRED_ERROR_BAD_PARAMETER;

	} 
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	return CRED_NO_ERROR;
} 

/*! \fn CRED_Errors_t AUDIO_Record_Stop(AUDIO_Enc *encode)
    \brief Stop record AAC process.
    \param AUDIO_Enc *encode a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_Record_Stop(AUDIO_Enc *encode) 
{	
	
	CRED_Errors_t err = CRED_NO_ERROR;
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->g_eAACRecorder_State == eAACRECORDER_STATE_STOPPED)
	{
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it is stopped\n");
		return CRED_ERROR_AUDIO_ENCODE_STOP;
	} 

	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	if (encode->s_bRecordThread_Alive || encode->s_bEncodeThread_Alive) 
	{
		pthread_mutex_lock(&encode->g_sRecorderStateMutex);
		encode->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		
		do 
		{
			struct timespec	sSleepTime = {0,0};
			nanosleep(&sSleepTime, NULL);
		} while (encode->s_bRecordThread_Alive || encode->s_bEncodeThread_Alive);
	}
	
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	encode->g_eAACRecorder_State = eAACRECORDER_STATE_STOPPED;
	AUDIO_TRACE_INFO("stopped Encode\n");
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	
	return err;
} 

/*! \fn CRED_Errors_t AUDIO_Record_Pause(AUDIO_Enc *encode) 
    \brief Pause record AAC process.
    \param AUDIO_Enc *encode a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_Record_Pause(AUDIO_Enc *encode) 
{
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->g_eAACRecorder_State != eAACRECORDER_STATE_RECORDING)
	{
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio state it must be playing\n");
		return CRED_ERROR_AUDIO_ENCODE_PAUSE;
	} 
	
	encode->g_eAACRecorder_State = eAACRECORDER_STATE_TOPAUSE;
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
	
	while (1) 
	{
		struct timespec	sSleepTime = { 0, 0 };
		nanosleep(&sSleepTime, NULL);

		if (encode->g_eAACRecorder_State == eAACRECORDER_STATE_PAUSED) 
		{
			break;
		} 

	} 

	return CRED_NO_ERROR;
}

/*! \fn CRED_Errors_t AUDIO_Record_Resume(AUDIO_Enc *encode) 
    \brief Resume record AAC process.
    \param AUDIO_Enc *encode a structure that contains all 
			the inputs/outputs AAC parameters.
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t AUDIO_Record_Resume(AUDIO_Enc *encode) 
{
	if (encode == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio encode\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&encode->g_sRecorderStateMutex);
	if (encode->g_eAACRecorder_State != eAACRECORDER_STATE_PAUSED) 
	{
		pthread_mutex_unlock(&encode->g_sRecorderStateMutex);
		AUDIO_TRACE_ERROR( "Bad audio encode state it must be paused\n");
		return CRED_ERROR_AUDIO_ENCODE_RESUME;
	} 

	encode->g_eAACRecorder_State = eAACRECORDER_STATE_TORESUME;
	
	pthread_mutex_unlock(&encode->g_sRecorderStateMutex);

	while (1)
	{
		struct timespec	sSleepTime = {0,0};
		nanosleep(&sSleepTime, NULL);

		if (encode->g_eAACRecorder_State == eAACRECORDER_STATE_RECORDING) 
		{

			break;
		} 

	} 

	
	return CRED_NO_ERROR;
}


/*! \fn void s_RecordThread(void* encode) 	
    \brief Thread to record AAC frames.
    \param void* encode a structure that contains all the inputs/outputs AAC parameters.
    \retval void:any value to return.
*/

void s_RecordThread(void* encode) 
{
	AUDIO_Enc *encode_th = (AUDIO_Enc *)encode;
	encode_th->s_bRecordThread_Alive = true;
	int  number=0;
	int		i32ReadBytes;
	struct timespec	sSleepTime = { 0, 0};
	int		s_i32PCMFrame_WriteIdx = 0;
	int value_ptr;
	char	*pcPCMDst,*pcPCMSrc;
	CRED_Errors_t err;

	encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
	
	pthread_detach(pthread_self()); 
	
	pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
	
	err = s_OpenAudioRecDevice(AACRECORDER_SAMPLE_RATE, AACRECORDER_CHANNEL_NUM,&encode_th->s_i32DevDSP_BlockSize_enc);
	if (err != CRED_NO_ERROR)
	{
		AUDIO_TRACE_ERROR("Open Audio RecDevice failed %d\n",err);	
	}

	pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
	
	if (encode_th->s_i32DevDSP_BlockSize_enc > 0) 
	{

		char	*pcPCMBuf = malloc(encode_th->s_i32DevDSP_BlockSize_enc);

		if (pcPCMBuf == NULL) 
		{
			AUDIO_TRACE_ERROR("malloc() in Record thread \n");
			
			//~ err=Close_Audio_Record(*encode_th); 
			if (err != CRED_NO_ERROR) 
			{
				AUDIO_TRACE_ERROR("failed to Close_Audio_Record %d\n",err);	
			}
			goto EndOfRecordThread;
		} 

		unsigned int g_u32AACRecorder_RecFrameCnt = 0;
		while (1) 
		{

			pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_STOPPED) 
			{
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				break;
			}
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOSTOP) 
			{
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				break;
			} 
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOPAUSE && encode_th->input == file) 
			{	
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_PAUSED;
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				continue;
			} 
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOPAUSE && encode_th->input == micro) 
			{	
				close(encode_th->mixer_fd);
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_PAUSED;
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				continue;
			} 

			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_PAUSED) 
			{
				pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
				struct timespec	sSleepTime = { 0, 0 };
				nanosleep(&sSleepTime, NULL);
				continue;
			}
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TORESUME && encode_th->input == micro) 
			{		
				encode_th->mixer_fd = open("/dev/mixer", O_WRONLY);
				if (encode_th->mixer_fd < 0)
				{
					AUDIO_TRACE_ERROR("Open Audio RecDevice failed for resume encode %d\n",err);	
				}
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
			}
			
			if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TORESUME && encode_th->input == file) 
			{		
				encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
			}
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);

			//~ int		i32ReadBytes = read(encode_th->s_i32DevDSP, pcPCMBuf, encode_th->s_i32DevDSP_BlockSize_enc);
            i32ReadBytes = snd_pcm_readi(captureHandle, pcPCMBuf,audio_recording_frames );
			if (i32ReadBytes == -EPIPE) {
			  /* EPIPE means overrun */
			  fprintf(stderr, "overrun occurred\n");
			  snd_pcm_prepare(captureHandle);
			} else if (i32ReadBytes < 0) {
			  fprintf(stderr,
					  "error from read: %s\n",
					  snd_strerror(i32ReadBytes));
					  break;
			} else if (i32ReadBytes != (int)audio_recording_frames) {
			  fprintf(stderr, "short read, read %d frames\n", i32ReadBytes);
			}

			//int		i32ReadBytes = read(encode_th->s_i32DevDSP, pcPCMBuf, encode_th->s_i32DevDSP_BlockSize_enc);
            i32ReadBytes*=2;
			if(encode_th->input == file)
			{
			number ++;
				if (( number > encode_th->s_i32no) || (i32ReadBytes <= 0 ))
				{

					i32ReadBytes = 0; 
				do {
					nanosleep(&sSleepTime, NULL);
					}while (encode_th->s_i32PCMFrame_FullNum > 0);
			
               encode_th->s_bRecordThread_Alive = false;
			   encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
				}
			}
			else
			{

				if (i32ReadBytes <= 0 )
				{
				   i32ReadBytes = 0; 
					do 
					{
						nanosleep(&sSleepTime, NULL);
					}while (encode_th->s_i32PCMFrame_FullNum > 0);
					
				   encode_th->s_bRecordThread_Alive = false;
				   encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
				}
			}
			bool	bWarning = false;
			pcPCMSrc = pcPCMBuf;
			while (i32ReadBytes > 0) 
			{

				if (encode_th->s_i32PCMFrame_FullNum < PCMFRAME_BUFNUM-1) 
				{
					pcPCMDst = (char*)(encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_WriteIdx]) + encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx];
					//*pcPCMSrc = pcPCMBuf;
					int	i32FillBytes = PCMFRAME_BUFSIZE - encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx];
					
					if (i32FillBytes > i32ReadBytes)
						i32FillBytes = i32ReadBytes;
					
					memcpy(pcPCMDst, pcPCMSrc, i32FillBytes);
					pcPCMDst += i32FillBytes;
					pcPCMSrc += i32FillBytes;

					encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx] += i32FillBytes;
					if (encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_WriteIdx] >= PCMFRAME_BUFSIZE)
					{
						g_u32AACRecorder_RecFrameCnt++;

						s_i32PCMFrame_WriteIdx++;
						if ( s_i32PCMFrame_WriteIdx == PCMFRAME_BUFNUM )
							s_i32PCMFrame_WriteIdx = 0;
			
						pcPCMDst = (char*)encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_WriteIdx]; 
						encode_th->s_i32PCMFrame_FullNum++;
					} 
					else {
					} 
					i32ReadBytes -= i32FillBytes;
				}
				else 
				{

					if (bWarning == false) 
					{
						bWarning = true;
					}
					
					struct timespec	sSleepTime = { 0, 0 };
					nanosleep(&sSleepTime, NULL);
				} 
			} 

			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
		} 
						printf("Exiting recording ...");

	} 
EndOfRecordThread:
      printf("closing record device ...");
    err = AUDIO_Record_Release(encode_th);
    printf("done\n");
	if (err != CRED_NO_ERROR) 
	{
		AUDIO_TRACE_ERROR("failed to release allocated buffers for record %d\n",err);	
	}


	encode_th->s_bRecordThread_Alive = false;
	pthread_exit(&value_ptr);
	AUDIO_TRACE_INFO("Exit Record thread \n");
} 

/*! \fn void s_EncodeThread(void* encode) 	
    \brief Thread to encode AAC frames.
    \param void* encode a structure that contains all the inputs/outputs AAC parameters.
    \retval void:any value to return.
*/
void s_EncodeThread(void* encode) 
{
      
	AUDIO_Enc *encode_th = (AUDIO_Enc *)encode;
	encode_th->s_bEncodeThread_Alive = true;
	int		s_i32PCMFrame_ReadIdx = 0;
	pthread_detach(pthread_self()); 
	char	*pcEncFrameBuf = malloc(ENCFRAME_BUFSIZE);
	int value_ptr;
	CRED_Errors_t err = CRED_NO_ERROR;
	int	i32Index;
	AUDIO_TRACE_INFO("AAC Recorder: START\n"); 
	if (pcEncFrameBuf == NULL) 
	{
		AUDIO_TRACE_ERROR("error malloc() for pcEncFrameBuf \n");
		goto EndOfEncode;		
	} 

	S_AACENC sEncoder;
	
	encode_th->total_frame_rec = 0;
	
	sEncoder.m_u32SampleRate = AACRECORDER_SAMPLE_RATE;
	sEncoder.m_u32ChannelNum = AACRECORDER_CHANNEL_NUM;
	sEncoder.m_u32BitRate = AACRECORDER_BIT_RATE * sEncoder.m_u32ChannelNum;
	sEncoder.m_u32Quality = AACRECORDER_QUALITY;
	sEncoder.m_bUseAdts = true;
	sEncoder.m_bUseMidSide = false;
	sEncoder.m_bUseTns = false;
	
	E_AACENC_ERROR	eAACEnc_Error = AACEnc_Initialize(&sEncoder);

	if (eAACEnc_Error != eAACENC_ERROR_NONE) 
	{
		AUDIO_TRACE_ERROR("AAC Recorder: Fail to initialize AAC Encoder \n", eAACEnc_Error);
		goto EndOfEncode;		
	} 
	
	unsigned int g_u32AACRecorder_EncFrameCnt = 0;
	encode_th->total_EncFrameSize = 0;

	while(1)
	{
		pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOSTOP)
		{
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			break;
		}
		
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_STOPPED) 
		{
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			break;
		} 
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TOPAUSE)
		{	
			encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_PAUSED;
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			continue;
		} 
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_PAUSED) 
		{
			pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
			continue;
		}
		if (encode_th->g_eAACRecorder_State == eAACRECORDER_STATE_TORESUME) 
		{		
			encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_RECORDING;
		}
		pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
		
		if (encode_th->s_i32PCMFrame_FullNum > 0) 
		{			
			int	i32EncFrameSize;
			eAACEnc_Error = AACEnc_EncodeFrame((short*)encode_th->s_apcPCMFrame_Buf[s_i32PCMFrame_ReadIdx], pcEncFrameBuf,encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_ReadIdx] /*ENCFRAME_BUFSIZE*/, &i32EncFrameSize); // pcEncFrameBuf elements à ecrire
			if (eAACEnc_Error != eAACENC_ERROR_NONE) 
			{
				AUDIO_TRACE_ERROR("AAC Recorder: Fail to encode \n", eAACEnc_Error);
				
				MsEvent.As_event = CRED_AS_MULTIMEDIA; 
				Multimedia_event.multimedia_action = CRED_MS_ENC_FRAME_AAC;
				MsEvent.event_detail = (uint8_t*)&(Multimedia_event.multimedia_action);
				Multimedia_event.event_detail = NULL;
				//MS_Notify(MsEvent);
				if(audio_ms_notify)
					(*audio_ms_notify)(MsEvent);
				break;	
			}
			 
			encode_th->total_frame_rec++;
		
			encode_th->total_EncFrameSize = encode_th->total_EncFrameSize + i32EncFrameSize; 
			
			if (encode_th->total_EncFrameSize > encode_th->audio_size_file_rec) 
			{
				AUDIO_TRACE_INFO("AAC Recorder: encode is stopped the maximum file size is reached \n"); 
				encode_th->total_EncFrameSize = encode_th->total_EncFrameSize -i32EncFrameSize; 
				//~ MsEvent.As_event = CRED_AS_MULTIMEDIA;
				//~ Multimedia_event.multimedia_action = CRED_MS_FULL_SIZE_FILE_AAC;
				//~ MsEvent.event_detail = (uint8_t*)&(Multimedia_event.multimedia_action);
				//~ Multimedia_event.event_detail = NULL;
				//~ //MS_Notify(MsEvent);
				//~ if(audio_ms_notify)
					//~ (*audio_ms_notify)(MsEvent);
				break;
			}
			
			if (fwrite(pcEncFrameBuf, 1, i32EncFrameSize, encode_th->s_psAACFile_enc) != i32EncFrameSize) // write AACfile
			{
				AUDIO_TRACE_ERROR("fwrite() in AACFile \n");
				break;
			} 
			
			g_u32AACRecorder_EncFrameCnt++;
			encode_th->s_ai32PCMFrame_PCMSize[s_i32PCMFrame_ReadIdx] = 0;
			s_i32PCMFrame_ReadIdx++;
			if ( s_i32PCMFrame_ReadIdx == PCMFRAME_BUFNUM )
    			s_i32PCMFrame_ReadIdx = 0;

			//if (s_i32PCMFrame_ReadIdx == 0)
				//sync();
			encode_th->s_i32PCMFrame_FullNum--;
		} 
		else if (encode_th->s_bRecordThread_Alive) 
		{		
			struct timespec	sSleepTime = { 0, 0 };
			nanosleep(&sSleepTime, NULL);
		}
		else 
		{
 		    encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_TOSTOP;
			break;
		} 
	} 
	
	EndOfEncode:
	//~ err = AUDIO_Record_Release(encode_th);
	if (err != CRED_NO_ERROR) 
	{
		AUDIO_TRACE_ERROR("failed to release allocated buffers for record %d\n",err);	
	}

	AACEnc_Finalize();
	if (pcEncFrameBuf) 
	{
		free(pcEncFrameBuf);
	}
	encode_th->s_bRecordThread_Alive = false; 
	pthread_mutex_lock(&encode_th->g_sRecorderStateMutex);
	encode_th->g_eAACRecorder_State = eAACRECORDER_STATE_STOPPED; 
	pthread_mutex_unlock(&encode_th->g_sRecorderStateMutex);
	//~ if (err != CRED_NO_ERROR) 
	//~ {
		//~ AUDIO_TRACE_ERROR("failed close audio device for record %d\n",err);	
	//~ } 
	encode_th->s_bEncodeThread_Alive = false;
	
	err = audio_aac_set_info(encode_th);
	if (err != CRED_NO_ERROR)
	{
		AUDIO_TRACE_ERROR("audio_aac_set_info failed %d\n",err);	
	}
	pthread_exit(&value_ptr);
	AUDIO_TRACE_INFO("Exit Encode thread \n");
} 


CRED_Errors_t AUDIO_Set_Input_Interface(E_AUR_MIC_SEL Input_Interface) 
{
	int audio_fd;
	
	audio_fd = open("/dev/dsp1",O_RDONLY,0);
	if(audio_fd<0)
	{
		AUDIO_TRACE_ERROR("cannot open audio device\n");
		return CRED_ERROR_BAD_FD;
	}

	if(0>ioctl(audio_fd,SNDCTL_DSP_INPUT_INTERFACE, &Input_Interface))
	{
		AUDIO_TRACE_ERROR("SNDCTL_DSP_INPUT_INTERFACE ioctl fail\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	
	close(audio_fd);
}
 /*! \fn 	CRED_Errors_t ADC_Init(void)
    \brief 	Audio device init .
    \param  none .
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t audio_driver_init(CRED_MW_Errors_t *audio_ms_notify_ptr)
{
	if(audio_ms_notify_ptr == NULL)
	{
		ADC_TRACE_ERROR("the event function Pointer is NULL\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio_ms_notify = audio_ms_notify_ptr;

	return CRED_NO_ERROR;
}

CRED_Errors_t AAC_Files_Decode(AUDIO *audio) 
{
	CRED_Errors_t err = CRED_NO_ERROR;
	pthread_t 	sDecodeThread;
	FILE *on_site_file;
	char cmd_data[100];
	memset(cmd_data,0,sizeof(cmd_data));
	//sprintf(cmd_data,"rm %s",CRED_AUDIO_ON_SITE_FILE_PATH);
	//ShellCmd(cmd_data);
	//sync();
	on_site_file = fopen(CRED_AUDIO_ON_SITE_FILE_PATH,"w+");
	fclose(on_site_file);
	//sync();

	if(audio_run == 1)
	{
		AUDIO_TRACE_ERROR( "Audio Device Used\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	audio_run = 1;
	if (audio == NULL)
	{
		AUDIO_TRACE_ERROR( "empty struct audio\n");
		audio_run = 0;
		return CRED_ERROR_BAD_PARAMETER;
	}
	pthread_mutex_lock(&audio->g_sPlayerStateMutex);
	if (audio->s_bDecodeThread_Alive)
	{
		AUDIO_TRACE_ERROR( "Audio Device Used");
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		audio_run = 0;
		return CRED_ERROR_BAD_PARAMETER;
	}else
	{
		AUDIO_TRACE_INFO("\n Audio device not used %d\n",audio->s_bDecodeThread_Alive);
		audio->s_bDecodeThread_Alive = true;
	}
	
	if (audio->g_eAACPlayer_State != eAACPLAYER_STATE_STOPPED) 
	{
		AUDIO_TRACE_ERROR("erreur FileName or play state \n");
		pthread_mutex_unlock(&audio->g_sPlayerStateMutex);
		audio->s_bDecodeThread_Alive = false;
		audio_run = 0;
		return CRED_ERROR_BAD_PARAMETER;
	}

	audio->operation_type = AUDIO_DECODE_FILES;
	audio->s_bDecodeThread_Alive = true;

	pthread_mutex_unlock(&audio->g_sPlayerStateMutex);

	if (CRED_TOOLS_CreateTask("sDecodeThread", &sDecodeThread, 0x2000,s_DecodeThread, 99, SCHED_FIFO,(void *)audio)  < 0) 
	{ 
		audio->s_bDecodeThread_Alive = false;
		AUDIO_TRACE_ERROR(" erreur create decode thread");
		audio_run = 0;
		AUDIO_fd_Close(audio);
		return CRED_ERROR_BAD_PARAMETER;
	} 
	
	audio_run = 0;
	return err;
}
 /*! \fn 	CRED_Errors_t audio_get_dec_status(in *audio_dec_status)
    \brief 	Audio get status .
    \param  none .
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t audio_get_dec_status(int *audio_dec_status)
{
	if(audio_dec_status == NULL)
	{
		ADC_TRACE_ERROR("audio_dec_status Pointer is NULL\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	*audio_dec_status = decode_status;
	return CRED_NO_ERROR;
}
 /*! \fn 	CRED_Errors_t audio_get_play_status(int *audio_play_status)
    \brief 	Audio get status .
    \param  none .
    \retval CRED_NO_ERROR:function terminated successfully.
    \retval !CRED_NO_ERROR:function terminated with fail.
*/
CRED_Errors_t audio_get_play_status(int *audio_play_status)
{
	if(audio_play_status == NULL)
	{
		ADC_TRACE_ERROR("audio_dec_status Pointer is NULL\n");
		return CRED_ERROR_BAD_PARAMETER;
	}
	if(pcm_play_status || PlaybackThreadStatus)
	{
		*audio_play_status = 1;
	}else
	{
		*audio_play_status = 0;
	}
	return CRED_NO_ERROR;
}
