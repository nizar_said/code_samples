
#include "PCM.h"

int main()
{

	int buf;
	char cmd;
	PCM pcm;
	
			AUDIO_TRACE_TEST("AAC Decoder library %d.%02x Copyright (c) Nuvoton Technology Corp.\n"
		   "	1          :  Play  file\n"
		   "	2          :  Stop\n"
		   "	3          :  Resume\n"
		   "	4          :  Pause\n"
		   "	5          :  Exit\n"
			"	6          :  Record\n"
		   "	7          :  Pause_Record\n"
		   "	8          :  Resume_Record\n"
		   "	9          :  Stop_Record\n"
		   
		   );
			AUDIO_TRACE_TEST("\n**** Audio Test Program ****\n");
			//printf("\nRec Seconds:");
			AUDIO_TRACE_TEST("\nRec Seconds:");
			scanf("%10d",&pcm.seconds);
							
	while(1)
	{	
			cmd = getchar();
			
		switch(cmd) 
		{
			case 0x36:
			{	
				record_to_pcm_adc(&pcm);
				AUDIO_TRACE("record\n");	
			}
			break;
			
			case 0x31:
			{	
				void_play_pcm_single(&pcm);
				AUDIO_TRACE("Play\n");	
			}
			break;
			case 0x32:
			{
				stop_play(&pcm);
				AUDIO_TRACE("Stop play \n");
			}
			break;
			case 0x33:
			{
				pause_play(&pcm);
				AUDIO_TRACE("Pause play\n");
				
			}
			break;
			case 0x34:
			{
				resume_play(&pcm);
				AUDIO_TRACE("Resume play\n");
			}
			break;
			case 0x37:
			{
				pause_record(&pcm);
				AUDIO_TRACE("pause record\n");
			}
			break;
			case 0x38:
			{
				resume_record(&pcm);
				AUDIO_TRACE("Resume record\n");
			}
			break;
			case 0x39:
			{
				stop_record(&pcm);
				AUDIO_TRACE("Stop record\n");	
			}		
			break;
			case 0x35:
			{
				goto exit;
			}
			default:
			break;
		}
	}
exit:
	return 0;	
}


